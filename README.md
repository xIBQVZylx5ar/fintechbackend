# Fintech-BackEnd
Para probar el proyecto se necesita un cliente de Graphql 
o utilizar el propio del server que se encuentra en la ruta
http://localhost:5000/graphql


Al correr node helper.js se puede inicializar la linea de comandos de delfino 
Ejemplo:
- node helper.js s 
Obtiene todas las sucripciones en delfino de existentes en greenpay y las almacena en un localDB

## TODOS
-Mover estructura de archivos en type a Backoffice,Delfino,Commons
    ---->types
        ----->backoffice
        ----->delfino
        ----->commons
        ----->Actualizar el helper

-Mover DATE a TIME en sequelize para aquellos que sean DATETIME
-Agregar winston logger
-Revisar los usuarios que tienen una order con un next_payment > 2021:01:01 00:00:00
-Hacer configuracion del .vscode

Changes in read-me