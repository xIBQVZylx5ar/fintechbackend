const { dateToCostaRica } = require('../utils');

const toGql = (user) => {
  let formattedUser = {
    ...user,
  };
  formattedUser.created_at = dateToCostaRica(formattedUser.created_at);
  if (!formattedUser.store || !formattedUser.store[0]) {
    formattedUser.store = null;
  } else {
    formattedUser.store = formattedUser.store[0];
  }
  return formattedUser;
};
const cleanUserAndStore = (user, store) => {
  let newUser = {
    ...user,
  };
  if (store) {
    newUser.store = store;
  }
  if (newUser.password) {
    delete newUser.password;
  }
  return newUser;
};
module.exports = { toGql, cleanUserAndStore };
