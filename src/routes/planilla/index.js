const express = require('express');
const prisma = require('../../services/prisma');
const router = express.Router();

router.post('/new', async (req, res) => {
    const { profile_id, amount, trx_date, collaborators, user_id = 8 } = req.body;
    let userID = user_id;

    if(!profile_id || !amount || !trx_date || !collaborators){    
        return res.status(401).send("Parámetros insuficientes")
    }

    try{
        let data;
        let planilla;
        
        data = JSON.parse(collaborators).map((id) => ({
            user_id: userID,
            profile_id: parseInt(profile_id),
            amount: amount, 
            trx_date: new Date(trx_date),
            collaborator_id: parseInt(id),
        }))
        
        planilla = await prisma.payroll.createMany({data})

        if(planilla){
            return res.status(200).send("Inserción exitosa")
        } else {
            return res.status(400).send("Inserción fallida")
        }
    }catch(error){
        return res.status(400).send(error)
    }
})

router.put('/update', async (req, res) => {
    const { payroll_id, profile_id, amount, trx_date, collaborator_id, user_id=8 } = req.body;
    let userID = user_id;

    if(!payroll_id || !profile_id || !amount || !trx_date || !collaborator_id){
        return res.status(401).send("Parámetros insuficientes")
    }

    try{
        let data;
        let planilla;

        data = {
            user_id: userID,
            profile_id: parseInt(profile_id),
            amount: amount,
            trx_date: new Date(trx_date),
            collaborator_id: parseInt(collaborator_id)
        };

        planilla = await prisma.payroll.update({
            where: {
                payroll_id: parseInt(payroll_id)
            },
            data
        });

        if(planilla){
            return res.status(200).send("Inserción exitosa");
        }else{
            return res.status(400).send("Inserción fallida");
        }
    }catch(error){
        return res.status(400).send(error)
    }
})

router.delete('/remove', async (req, res) => {
    const { payroll_id } = req.query;
    try{
        let planilla;

        planilla = await prisma.payroll.delete({
            where: {
                payroll_id: parseInt(payroll_id)
            }
        })

        if(planilla) {
            return res.status(200).send("Inserción exitosa")
        }else{
            return res.status(400).send("Inserción fallida")
        }
    }catch(error){
        return res.status(400).send(error)
    }
})

router.get('/', async (req, res) => {
    const { profile_id, start_date, end_date } = req.query;    

    if(!profile_id){
        return res.status(401).send("Parámetros insuficientes")
    }

    let sDate; // start date 
    let eDate; // end date 

    const months = [
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Setiembre',
        'Octubre',
        'Noviembre',
        'Diciembre',
    ];
    const groupBy = (xs, key) => {
        return xs.reduce((rv, x) => {
        let newRv = rv;
        (newRv[x[key]] = newRv[x[key]] || []).push(x);
        return rv;
        }, {});
    };
    const report = [];

    try{
        /* DATE FORMAT */
        
        if (!start_date || !end_date) {
            formattedSDate = new Date().getFullYear() + '-01-01';
            formattedEDate = new Date().getFullYear() + '-12-31'
    
        } else {
            sDate = new Date(start_date)
            sDate.setMinutes(sDate.getMinutes() + sDate.getTimezoneOffset())
            eDate = new Date(end_date)
            eDate.setMinutes(eDate.getMinutes() + eDate.getTimezoneOffset())
            formattedEDate = date.format(eDate, 'YYYY-MM-DD');
            formattedSDate = date.format(sDate, 'YYYY-MM-DD');
    
        }
  
        let sqlReport;
        sqlReport = await prisma.$queryRaw(
            `SELECT EXTRACT(MONTH FROM "trx_date") as "month_date", EXTRACT(YEAR FROM "trx_date") as "year_date", SUM ("amount") as total 
            FROM public.payroll 
            WHERE "profile_id" = ${profile_id} AND "trx_date" BETWEEN '${formattedSDate}' AND '${formattedEDate}' group by "month_date", "year_date"`
        )
        const groupedSqlReport = groupBy(sqlReport, 'year_date');
        const reportYears = Object.keys(groupedSqlReport);
        let startDateParam, endDateParam;
        let formatedStartDate, formatedEndDate;
        for (let i = 0; i < reportYears.length; i++) {
            for (let j = 0; j < months.length; j++) {
              const monthExists = groupedSqlReport[reportYears[i]].find(
                (row) => row.month_date === j + 1
              );
                if (monthExists) {
                    startDateParam = new Date(
                        parseInt(reportYears[i], 10),
                        monthExists.month_date,
                        1
                    );
                    ///date parses to many results querry 
                    formatedStartDate = reportYears[i] + '-' + monthExists.month_date + '-01';
                    endDateParam = new Date(parseInt(reportYears[i], 10), monthExists.month_date, 0);
                    formatedEndDate = endDateParam.getFullYear() + '-' + (endDateParam.getMonth() + 1) + '-' + endDateParam.getDate();
                    let payrolls;

                    payrolls = await prisma.$queryRaw(
                        `SELECT p."payroll_id", p."trx_date", p."amount", c."collab_name"
                        FROM public.payroll p, public.collaborator c
                        WHERE p."collaborator_id" = c."collaborator_id"
                        AND p."profile_id" = ${parseInt(profile_id, 10)} 
                        AND p."trx_date" BETWEEN '${formatedStartDate}' AND '${formatedEndDate}' ORDER BY p."trx_date"`
                    )
                    /*payrolls = await prisma.$queryRaw(
                        `SELECT * FROM public.payroll`
                    )*/
                    report.push({
                        payrolls,
                        year: parseInt(reportYears[i], 10),
                        month: monthExists.month_date,
                        total: monthExists.total,
                        monthLabel: months[j],
                    });
                } else {
                    report.push({
                        payrolls: [],
                        year: parseInt(reportYears[i], 10),
                        month: j + 1,
                        total: 0,
                        monthLabel: months[j],
                    });
            }
        }
    }

    return res.status(200).send(report);
    }catch(error){
        return res.status(400).send(error)
    }
})

router.get('/unique', async(req, res) => {
    const { payroll_id } = req.query;

    if(!payroll_id){
        return res.status(401).send("Parámetros insuficientes")
    }

    try {
        let payroll;
        payroll = await prisma.payroll.findUnique({
            where: {
                payroll_id: parseInt(payroll_id)
            },
            include: {
                collaborator: true
            }
        })

        if(payroll){
            return res.status(200).send(payroll)
        } else {
            return res.status(400).send("Inserción fallida")
        }
    } catch(error){
        return res.status(400).send(error)
    }
})

router.post('/collaborator', async(req, res) => {
    const { name, user_id = 8 } = req.body;
    let userID = user_id

    if(!name){
        return res.status(401).send("Parámetros insuficientes")
    }

    try{
        let data;
        let collaborator;

        data = {
            user_id: userID,
            collab_name: name 
        }

        collaborator = await prisma.collaborator.create({data})

        if(collaborator){
            return res.status(200).send("Inserción exitosa")
        } else {
            return res.status(400).send("Inserción fallida")
        }

    }catch(error){
        return res.status(400).send(error)
    }
})

router.get('/collaborator', async (req, res) => {
    const { user_id = 8} = req.query;
    let userID = user_id

    try{
        let collaborator;
        collaborator = await prisma.collaborator.findMany({
            where: {
                user_id: userID
            }
        });

        return res.status(200).send(collaborator)
    }catch(error){
        return res.status(400).send(error)
    }
})

module.exports = router;