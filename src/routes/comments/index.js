const express = require("express");
const prisma = require('../../services/prisma');
const router = express.Router();

// CREAR UN NUEVO COMENTARIO
router.post('/new', async (req, res) => {
    const { content, postId, user_id = 8 } = req.body;
    //const { postId } = req.query

    let userID = user_id;

    if(!content || !postId){
        return res.status(401).send("Parámetros insuficientes")
    }

    try{
        let data;
        let comment;

        data = {
            user_id: userID,
            content: content,
            post_id: parseInt(postId)
       }

        comment = await prisma.comments.create({data})

        if(comment){
            return res.status(200).send("Inserción exitosa")
        } else {
            return res.status(400).send("Inserción fallida")
        }

    } catch (error){
        return res.status(400).send(error)
    }
});

// ACTUALIZAR EL COMENTARIO
router.put('/update', async (req, res) => {
    const { content, user_id = 8, commentId, postId } = req.body;
    //const { postId } = req.query;

    let userID = user_id;

    if(!content || !commentId || !postId){
        return res.status(401).send("Parámetroes insuficientes");
    }

    try{
        let data;
        let comment;

        data = {
            user_id: userID,
            content: content,
            post_id: parseInt(postId)
        }

        comment = await prisma.comments.update({
            where: {
                id_comment: parseInt(commentId)
            },
            data
        })

        if(comment){
            return res.status(200).send("Inserción exitosa")
        } else {
            return res.status(400).send("Inserción fallida")
        }
    } catch(error){
        return res.status(400).send(error)
    }

});

// ELIMINAR POR ID EL COMMENTARIO
router.delete('/remove', async (req, res) => {
    const { commentId } = req.query;

    if(!commentId){
        return res.status(401).send("Parámetros insuficientes")
    }

    try{
        let comment;

        comment = await prisma.comments.delete({
            where: {
                id_comment: parseInt(commentId)
            }
        })

        if(comment){
            return res.status(200).send("Inserción exitosa");
        } else {
            return res.status(400).send("Inserción fallida")
        }
    } catch(error){
        return res.status(400).send(error)
    }
})

// LAMAR A TODOS LOS COMENTARIOS
router.get('/', async (req, res) => {
    let commentsAll;
    try{
        commentsAll = await prisma.comments.findMany()

        return  res.status(200).send(commentsAll)
    } catch (error) {
        return res.status.send(error)
    }
})

module.exports = router;