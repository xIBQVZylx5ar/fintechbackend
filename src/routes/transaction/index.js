const express = require('express');
const prisma = require('../../services/prisma');
const constants = require('../../utils/constants');
const logger = require('../../utils/logger/');
const router = express.Router();
const date = require('date-and-time');
const { user } = require('../../services/prisma');
/*-------------------------------------------------------*/
/*  Default insert method. it receive params:  
transaction_type to know in which table make a post
data: it is the payload with the information to insert and is contained in the req.body variable 

*/
/*-------------------------------------------------------*/

router.post('/new', async (req, res) => {

  /// DESCOMENTAR CUANDO YA ESTÉ EL INICIO DE SESION PARA USAR EL CONTEX /////////
  /// DESERILIZAR LOS REQ !
  /*const user = req.ctx.user;
 if (!user) {
   return res.status(401).send({ message: 'Usuario no autenticado' });
 }*/



  const { transaction_type, user_id = 8 } = req.body;

  let userID = user_id; // CAMBIAR EL USER ID CUANDO YA SE USE EL CONTEX POR user 
  // Validations params
  if (!transaction_type) {
    return res.status(400).send("Parámetros insuficientes");
  }

  try {
    let data, context;
    switch (transaction_type) {
      case constants.TYPE_PERSONAL_REVENUE_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX:


        // Validations params  
        if (!req.body.profile_id || !req.body.amount || !req.body.trx_date || !req.body.destination_id || !req.body.origin_id || !req.body.description) {
          return res.status(400).send("Parámetros insuficientes en tipos de datos");
        }

        // Construct Data object to insert
        data = {
          user_id: userID,
          profile_id: req.body.profile_id,
          amount: req.body.amount,
          trx_date: new Date(req.body.trx_date),
          destination_id: parseInt(req.body.destination_id),
          origin_id: parseInt(req.body.origin_id),
          description: req.body.description
        }

        // Database insertion
        context = await prisma.revenue.create({
          data
        });

        break;

      case constants.TYPE_PERSONAL_EXPENSES_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_CAPEX_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_COGS_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_SGA_TRX:

        // Validations params
        if (!req.body.profile_id || !req.body.exp_type || !req.body.amount || !req.body.trx_date) {
          return res.status(400).send("Parámetros basicos insuficientes en tipos de datos");
        }




        data = {
          user_id: userID,
          profile_id: req.body.profile_id,
          exp_type: req.body.exp_type,
          amount: req.body.amount,
          trx_date: new Date(req.body.trx_date),
          supplier: req.body.supplier,
          iva_percentage: req.body.percentage,
          bill_number: req.body.bill_number,
          id_exp_cat: (req.body.id_exp_cat ? parseInt(req.body.id_exp_cat) : null),
          description: req.body.description,
          destination_id: parseInt(req.body.destination_id)
        }

        // Database insertion
        context = await prisma.expenses.create({
          data
        });

        break;

    case constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX:
    case constants.TYPE_PROFESSIONAL_BUSINESS_CXP_TRX:

      // Validations params
      if (!req.body.profile_id || !req.body.client_supplier || !req.body.amount || !req.body.trx_date|| !req.body.description) {
        return res.status(400).send("Parámetros basicos insuficientes en tipos de datos");
      }
      const cxType = (transaction_type === constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX ? 1 : 2);

      data = {
        user_id: userID,
        profile_id: req.body.profile_id,
        amount: req.body.amount,
        trx_date: new Date(req.body.trx_date),
        client_supplier: req.body.client_supplier,
        state: false,
        description: req.body.description,
        cx_type: cxType
      }

      context = await prisma.cxc_cxp.create({
        data
      });

      break;

      default:
        console.log('Invalid Case');
        break;
    }

    if (context) {
      return res.status(200).send("Inserción exitosa");
    } else {
      return res.status(400).send("Inserción fallida");
    }

  } catch (error) {

    logger.error({
      message: 'Exception error',
      error,
      func: 'new-transaction',
      tags: 'transaction',
      extras: { userId: user_id }
    });
    return res.status(400).send("Exception error");
  }
});

router.put('/update', async (req, res) => {
  /*const user = req.ctx.user;
  if (!user) {
    return res.status(401).send({ message: 'Usuario no autenticado' });
  }*/

  const { transaction_type, id,
    amount,
    trx_date,
    destination_id,
    origin_id,
    description, id_exp_cat , bill_number, iva_percentage ,supplier, client_supplier, exp_type} = req.body;

  let data, context;
  try {
    switch (transaction_type) {

      case constants.TYPE_PERSONAL_REVENUE_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX:

        // Desestructuración del body con la información a guardar
        const {

        } = req.body;

        // Validations params
        if (!id || !amount || !trx_date || !destination_id || !origin_id || !description) {
          return res.status(400).send("Parámetros insuficientes");
        }

        // Construir objeto Data a actualizar

        data = {
          amount: amount, trx_date: new Date(trx_date),
          destination_id: parseInt(destination_id), origin_id: parseInt(origin_id),
          description: description
        }
        // Database update
        context = await prisma.revenue.update({
          where: {
            revenue_id: parseInt(id)
          },
          data
        }).catch((err) => {

          return res.status(400).send(err);

        });

        break;

      case constants.TYPE_PERSONAL_EXPENSES_TRX:
        if (!id || !amount || !trx_date || !destination_id || !description || !id_exp_cat) {
          return res.status(400).send("Parámetros insuficientes");
        }

        data ={      
          amount : amount,  
          trx_date : trx_date,
          id_exp_cat : id_exp_cat, 
          description : description,
          destination_id : destination_id
        }
        context = await expenses.update({
          where: {
            id_expense: parseInt(id)
          },
          data
        }).catch((err) => {

          return res.status(400).send(err);

        });
        break;
      case constants.TYPE_PROFESSIONAL_BUSINESS_COGS_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_SGA_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_CAPEX_TRX:
      
        
        if (!id || !amount || !trx_date || !destination_id || !description || !supplier ) {
          return res.status(400).send("Parámetros insuficientes");
        }

        data ={      
          amount : amount,  
          trx_date : trx_date,
          bill_number : bill_number, 
          description : description,
          destination_id : destination_id, 
          supplier: supplier, 
          iva_percentage: iva_percentage
        }
        context = await prisma.expenses.update({
          where: {
            id_expense: parseInt(id, 10)
          },
          data
        }).catch((err) => {

          return res.status(400).send(err);

        });
        break;

      case constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_CXP_TRX:

        const { state } = req.body;

        if (state) {
          // Validations params
          if (!id) {
            return res.status(400).send("Parámetros insuficientes");
          }
          let newTransaction;

          const cxc_cxp = await prisma.cxc_cxp.findUnique({
            where: {
              cx_id: parseInt(id, 10),
            }
          }).catch((err) => {
            return res.status(400).send(err);
          });

          if (cxc_cxp) {
            if (cxc_cxp.cx_type === 1) {
              
              // Validations params
              if (!destination_id || !origin_id) {
                return res.status(400).send("Parámetros insuficientes");
              }

              const data = {
                user_id: cxc_cxp.user_id,
                profile_id: cxc_cxp.profile_id,
                amount: cxc_cxp.amount,
                trx_date: cxc_cxp.trx_date,
                destination_id: destination_id,
                origin_id: origin_id,
                description: cxc_cxp.description,
                iva_percentage: null
              }
              
              newTransaction = await prisma.revenue.create({
                data
              })
              .catch((err) => {
                return res.status(400).send(err);
              });;
            } else {
              // Validations params
              if (!exp_type || !bill_number || !destination_id) {
                return res.status(400).send("Parámetros insuficientes");
              }
              const data = {
                user_id: cxc_cxp.user_id,
                profile_id: cxc_cxp.profile_id,
                exp_type: exp_type,
                amount: cxc_cxp.amount,
                trx_date: cxc_cxp.trx_date,
                supplier: cxc_cxp.supplier,
                bill_number: bill_number,
                id_exp_cat: null,
                destination_id: destination_id,
                description: cxc_cxp.description,
                iva_percentage: null
              }
              newTransaction = await prisma.expenses.create({data});
            }
          } else {
            return res.status(400).send("La dato que desea modificar no existe");
          }

          if (newTransaction) {
            context = await prisma.cxc_cxp.delete({
              where: {
                cx_id: parseInt(id, 10)
              }
            }).catch((err) => {
              return res.status(400).send(err);
            });
          }

        } else {
          // Validations params
          if (!amount || !trx_date || !client_supplier || !description) {
            return res.status(400).send("Parámetros insuficientes");
          }
          
          data = {
            amount,
            trx_date,
            client_supplier,
            description
          }

          context = await prisma.cxc_cxp.update({
            where: {
              cx_id: parseInt(id, 10)
            }, data
          }).catch((err) => {
            return res.status(400).send(err);
          });
        }

        break;

      default:
        break;
    }

    if (context) {
      return res.status(200).send("Actualización exitosa");
    } else {
      return res.status(400).send("Actualización fallida");
    }

  } catch (error) {

    /* logger.error({
      message: 'Exception error',
      error,
      func: 'update',
      tags: 'transaction',
      extras: { userId: user_id }
    }); */

    return res.status(400).send("Exception error");
  }
});

router.delete('/remove', async (req, res) => {

  /*const user = req.ctx.user;
  if (!user) {
    return res.status(401).send({ message: 'Usuario no autenticado' });
  }*/

  const { transactionId, transactionType } = req.query;
  if (!transactionId) {
    return res.status(401).send({ message: 'Parámetros insuficientes' });
  }

  // Realizar la validación de usuario

  let removing;

  switch (transactionType) {

    case constants.TYPE_PERSONAL_REVENUE_TRX:
    case constants.TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX:
      // Database elimination
      removing = await prisma.revenue.delete({
        where: {
          revenue_id: parseInt(transactionId, 10)
        }
      }).catch((err) => {
        return res.status(400).send(err);
      });
      break;

    case constants.TYPE_PERSONAL_EXPENSES_TRX:
    case constants.TYPE_PROFESSIONAL_BUSINESS_CAPEX_TRX:
    case constants.TYPE_PROFESSIONAL_BUSINESS_COGS_TRX:
    case constants.TYPE_PROFESSIONAL_BUSINESS_SGA_TRX:

      // Database elimination
      removing = await prisma.expenses.delete({
        where: {
          id_expense: parseInt(transactionId, 10)
        }
      });
      break;
    case constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX:
    case constants.TYPE_PROFESSIONAL_BUSINESS_CXP_TRX:

      const exist = prisma.cxc_cxp.findFirst({
        where: {
          cx_id: parseInt(transactionId, 10)
        }
      });
      // Database elimination
      if (exist) {
        removing = await prisma.cxc_cxp.delete({
          where: {
            cx_id: parseInt(transactionId, 10)
          }
        });
      } else {
        return res.status(404).send("La tansacción de desea eliminar no existe");
      }
      
      break;

    default:
      console.log('Invalid Case');
      break;
  }

  if (removing) {
    return res.status(200).send("Eliminación realizada");
  } else {
    return res.status(400).send("Eliminación fallida");
  }

});

/*-----------------------------*/
/*Default get method, receives params:
profile_id 
start_date start date to  the query
end_date final date

*/
/*------------------------------ */

router.get('/', async (req, res) => {

  //RECORDAR DESCOMENTAR CON EL USO DEL INICIO DE SESION Y EL CONTEX
  /* const user = req.ctx.user;
  if (!user) {
    return res.status(401).send({ message: 'Usuario no autenticado' });
  } */

  //destructuration of parms 
  const { transaction_type, profile_id, start_date, end_date } = req.query;

  //validation
  if (!transaction_type || !profile_id) {
    return res.status(400).send("Parámetros insuficientes");
  }
  let sDate; // start date 
  let eDate; // end date 

  const months = [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Setiembre',
    'Octubre',
    'Noviembre',
    'Diciembre',
  ];
  const groupBy = (xs, key) => {
    return xs.reduce((rv, x) => {
      let newRv = rv;
      (newRv[x[key]] = newRv[x[key]] || []).push(x);
      return rv;
    }, {});
  };
  const report = [];

  try {
    /* DATE FORMAT */
    if (!start_date || !end_date) {
      formattedSDate = new Date().getFullYear() + '-01-01';
      formattedEDate = new Date().getFullYear() + '-12-31'

    } else {
      sDate = new Date(start_date)
      sDate.setMinutes(sDate.getMinutes() + sDate.getTimezoneOffset())
      eDate = new Date(end_date)
      eDate.setMinutes(eDate.getMinutes() + eDate.getTimezoneOffset())
      formattedEDate = date.format(eDate, 'YYYY-MM-DD');
      formattedSDate = date.format(sDate, 'YYYY-MM-DD');

    }

    let extype;
    let sqlReport;
    switch (transaction_type) {
      /* table assignation */
      case constants.TYPE_PERSONAL_REVENUE_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX:
        sqlReport = await prisma
          .$queryRaw(
            `SELECT EXTRACT(MONTH FROM "trx_date") as "month_date", EXTRACT(YEAR FROM "trx_date") as "year_date", SUM ("amount") as total FROM public.revenue WHERE "profile_id" = ${profile_id} AND "trx_date" BETWEEN '${formattedSDate}' AND '${formattedEDate}' group by "month_date", "year_date"`
          )
          .catch((err) => {
            return res.status(400).send(err);
          });

        break;
      case constants.TYPE_PERSONAL_EXPENSES_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_CAPEX_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_COGS_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_SGA_TRX:

        extype = transaction_type == "Expenses" ? 1 :
          extype = transaction_type == "COGS" ? 2 :
            transaction_type == "SG&A" ? 3 :
              transaction_type == "CAPEX" ? 4 : null;

        sqlReport = await prisma
          .$queryRaw(
            `SELECT EXTRACT(MONTH FROM "trx_date") as "month_date", EXTRACT(YEAR FROM "trx_date") as "year_date", SUM ("amount") as total FROM public.expenses WHERE "profile_id" = ${profile_id} AND "exp_type" = ${extype} AND "trx_date" BETWEEN '${formattedSDate}' AND '${formattedEDate}' group by "month_date", "year_date"`
          )
          .catch((err) => {
            return res.status(400).send(err);
          });
        break;

      case constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_CXP_TRX:
        const cxType = (transaction_type === constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX ? 1 : 2);

        sqlReport = await prisma
        .$queryRaw(
          `SELECT EXTRACT(MONTH FROM "trx_date") as "month_date", EXTRACT(YEAR FROM "trx_date") as "year_date", SUM ("amount") as total FROM public.cxc_cxp WHERE "profile_id" = ${profile_id} AND  cx_type = ${cxType} AND "trx_date" BETWEEN '${formattedSDate}' AND '${formattedEDate}' group by "month_date", "year_date"`
        )
        .catch((err) => {
          return res.status(400).send(err);
        });

        break;
      default:
        console.log('Invalid option');
        break;
    }

    // PRINCIPAL QUERY 

    const groupedSqlReport = groupBy(sqlReport, 'year_date');
    const reportYears = Object.keys(groupedSqlReport);
    let startDateParam, endDateParam;
    let formatedStartDate, formatedEndDate;
    for (let i = 0; i < reportYears.length; i++) {
      for (let j = 0; j < months.length; j++) {
        const monthExists = groupedSqlReport[reportYears[i]].find(
          (row) => row.month_date === j + 1
        );
        if (monthExists) {
          startDateParam = new Date(
            parseInt(reportYears[i], 10),
            monthExists.month_date,
            1
          );
          ///date parses to many results querry 
          formatedStartDate = reportYears[i] + '-' + monthExists.month_date + '-01';
          endDateParam = new Date(parseInt(reportYears[i], 10), monthExists.month_date, 0);
          formatedEndDate = endDateParam.getFullYear() + '-' + (endDateParam.getMonth() + 1) + '-' + endDateParam.getDate();
          let transactions;
          // DIFFERENT QUERIES TO DATABASE BY TRANSACTION TYPE (REVENUE/ EXPENSES) AND EXPENSES TYPE 
          switch (transaction_type) {
            case constants.TYPE_PERSONAL_REVENUE_TRX:
            case constants.TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX:
              transactions = await prisma
                .$queryRaw(
                  `SELECT r.revenue_id, r."trx_date", r."amount", r."description", o."description" as origin, d."description" as destination 
                    FROM public.revenue r, public.origin o, public.destination d 
                    WHERE r."origin_id" = o."origin_id" 
                    AND r."destination_id" = d."destination_id" 
                    AND r."profile_id" = ${parseInt(profile_id, 10)} 
                    AND r."trx_date" BETWEEN '${formatedStartDate}' AND '${formatedEndDate}' ORDER BY r."trx_date"`
                )
                .catch((err) => {
                  return res.status(400).send(err);
                });
              break;
            case constants.TYPE_PERSONAL_EXPENSES_TRX:
              transactions = await prisma
                .$queryRaw(
                  `SELECT r.id_expense,  r.amount, r.trx_date, r.bill_number, c.description as "category", r.id_exp_cat, r.description, d.description as "destination" ,r.destination_id 
                  FROM public.expenses r, public.expcat c, public.destination d
                  WHERE c.id_exp_cat = r.id_exp_cat AND d.destination_id = r.destination_id AND d.user_id = r.user_id
                  AND r.profile_id = ${profile_id} 
                  AND r."trx_date" BETWEEN  '${formatedStartDate}' AND '${formatedEndDate}' ORDER BY r.trx_date`
                )
                .catch((err) => {
                  return res.status(400).send(err);
                });
              break;
            // TYPE OF EXPENSE PROFESSIONAL SERVICES AND BUSINESS
            case constants.TYPE_PROFESSIONAL_BUSINESS_COGS_TRX:
            case constants.TYPE_PROFESSIONAL_BUSINESS_CAPEX_TRX:
            case constants.TYPE_PROFESSIONAL_BUSINESS_SGA_TRX:
              extype = transaction_type == "COGS" ? 2 :
                transaction_type == "SG&A" ? 3 :
                  transaction_type == "CAPEX" ? 4 : null;
              transactions = await prisma
                .$queryRaw(
                  `SELECT e.id_expense,  t.id_type, t.description as "expense_type", e.amount, e.trx_date,e.supplier, e.iva_percentage, e.bill_number,e.description, d.destination_id, d.description as "destiny"
                  FROM public.expenses e, public.expenses_type t, public.destination d 
                  WHERE e.exp_type = t.id_type AND e.destination_id = d.destination_id AND e.profile_id = ${profile_id}  AND e.exp_type = ${extype} AND e.trx_date BETWEEN '${formatedStartDate}' AND '${formatedEndDate}' ORDER BY e.trx_date`
                )
                .catch((err) => {
                  return res.status(400).send(err);
                });
              break;

            case constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX:
            case constants.TYPE_PROFESSIONAL_BUSINESS_CXP_TRX:
              const cxType = (transaction_type === constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX ? 1 : 2);

              transactions = await prisma
              .$queryRaw(
                `SELECT r.cx_id, r.trx_date, r.amount, r.description 
                  FROM public.cxc_cxp r
                  WHERE r."profile_id" = ${parseInt(profile_id, 10)}
                  AND  cx_type = ${cxType}
                  AND r."trx_date" BETWEEN '${formatedStartDate}' AND '${formatedEndDate}' ORDER BY r."trx_date"`
              )
              .catch((err) => {
                return res.status(400).send(err);
              });
              break;
            default:
              break;
          }

          // ADDING TRX TO THE DETAILED REPORT
          report.push({
            transactions,
            year: parseInt(reportYears[i], 10),
            month: monthExists.month_date,
            total: monthExists.total,
            monthLabel: months[j],
          });
        } else {
          report.push({
            transactions: [],
            year: parseInt(reportYears[i], 10),
            month: j + 1,
            total: 0,
            monthLabel: months[j],
          });
        }
      }
    }

    return res.status(200).send(report);
  } catch (error) {
    console.log(error);
    /* logger.error({message : 'Exception error', 
                  error, 
                  func: 'get-transactions-by-date', 
                  tags: 'transaction',
                  extras: {userId: user_id}});
    return res.status(400).send("Exception error"); */
  }

});

/*------------------------------------------*/
/*Method to get a unique transactions from a table for showing details or charge data to update*/
/*------------------------------------------*/

router.get('/unique', async (req, res) => {
  //RECORDAR DESCOMENTAR PARA USAR EL CONTEX 
  /* const user = req.ctx.user;
  if (!user) {
    return res.status(401).send({ message: 'Usuario no autenticado' });
  } */
  const { transaction_type, id } = req.query;
 
  let toEdit;
  let result;
  try {
    switch (transaction_type) {
      /* table assignation */
      case constants.TYPE_PERSONAL_REVENUE_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_REVENUE_TRX:

        toEdit = await prisma.revenue.findUnique({
          where: {
            revenue_id: parseInt(id, 10)
          },
          include: {
            destination: {
              select: {
                description: true
              }
            },
            origin: {
              select: {
                description: true
              }
            }
          }
        }).catch((err) => {
          return res.status(400).send(err);
        })
        result = {
          trx_id: toEdit.revenue_id,
          amount: toEdit.amount,
          trx_date: toEdit.trx_date,
          origin: toEdit.origin.description,
          origin_id: toEdit.origin_id,
          destination: toEdit.destination.description,
          destination_id: toEdit.destination_id,
          description: toEdit.description
        };

        break;
      case constants.TYPE_PERSONAL_EXPENSES_TRX:
        toEdit = await prisma.expenses.findUnique({
          where: {
            id_expense: parseInt(id, 10)
          },
          include: {
            destination: {
              select: {
                description: true
              }
            },
            expcat: {
              select: {
                description: true
              }
            }
          }
        }).catch((err) => {
          return res.status(400).send(err);
        })
        result = {
          trx_id: toEdit.id_expense,
          amount: toEdit.amount,
          trx_date: toEdit.trx_date,
          bill_number: toEdit.bill_number,
          category: toEdit.expcat.description,
          id_exp_cat: toEdit.id_exp_cat,
          destination: toEdit.destination.description,
          destination_id: toEdit.destination_id,
          description: toEdit.description
        };

        break;
      case constants.TYPE_PROFESSIONAL_BUSINESS_CAPEX_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_COGS_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_SGA_TRX:
        toEdit = await prisma.expenses.findUnique({
          where: {
            id_expense: parseInt(id, 10)
          },
          include: {
            expenses_type: {
              select: {
                description: true
              }
            },
            destination: {
              select: {
                description: true
              }
            }
          }
        }).catch((err) => {
          return res.status(400).send(err);
        })
        result = {
          trx_id: toEdit.id_expense,
          id_type: toEdit.exp_type,
          expense_type: toEdit.expenses_type.description,
          amount: toEdit.amount,
          trx_date: toEdit.trx_date,
          supplier: toEdit.supplier,
          iva_percentage: toEdit.iva_percentage,
          bill_number: toEdit.bill_number,
          description: toEdit.description,
          destination_id: toEdit.destination_id,
          destiny: toEdit.destination.description
        };

        break;
        
      case constants.TYPE_PROFESSIONAL_BUSINESS_CXC_TRX:
      case constants.TYPE_PROFESSIONAL_BUSINESS_CXP_TRX:
        toEdit = await prisma.cxc_cxp.findUnique({
          where: {
            cx_id: parseInt(id, 10)
          }
        }).catch((err) => {
          return res.status(400).send(err);
        })

        result = {
          trx_id: toEdit.cx_id,
          amount: toEdit.amount,
          trx_date: toEdit.trx_date,
          client_supplier: toEdit.client_supplier,
          description: toEdit.description,
        };

        break;


      default:
        console.log('Invalid option');
        break;
    }
    return res.status(200).send(result);
  } catch (error) {
    console.log(error)
    /* logger.error({message : 'Exception error', 
                  error, 
                  func: 'get-transactions-by-date', 
                  tags: 'transaction',
                  extras: {userId: user_id}});
    return res.status(400).send("Exception error"); */
  }
});
router.get('/combos', async (req, res) => {
  //RECORDAR DESCOMENTAR PARA USAR EL CONTEX 
  /* const user = req.ctx.user;
  if (!user) {
    return res.status(401).send({ message: 'Usuario no autenticado' });
  } */
  const { combo_type, user, profile_id } = req.query;

  let destiny;
  let origin;
  let result;
  try {
    switch (combo_type) {
      /* table assignation */

      case 'destiny':
        destiny = await prisma.destination.findMany({
          where: {
            user_id: parseInt(user, 10),
            profile_id: parseInt(profile_id, 10)
          }
        }).catch((err) => {
          return res.status(400).send(err);
        })
        result = destiny.map((data) => {
          return {
            id: data.destination_id,
            description: data.description
          };
        });

        break;
      case 'origin':
        origin = await prisma.origin.findMany({
          where: {
            user_id: parseInt(user, 10),
            profile_id: parseInt(profile_id, 10)
          }
        }).catch((err) => {
          return res.status(400).send(err);
        })
        result = origin.map((data) => {
          return {
            id: data.origin_id,
            description: data.description
          };
        });

        break;
      case 'Expense':
        expenCat = await prisma.expcat.findMany({
          where: {
            user_id: parseInt(user, 10),
          }
        }).catch((err) => {
          return res.status(400).send(err);
        })
        result = expenCat.map((data) => {
          return {
            id: data.id_exp_cat,
            description: data.description
          };
        });

        break;
      default:
        console.log('Invalid option');
        break;
    }

    return res.status(200).send(result);
  } catch (error) {
    console.log(error)
    /* logger.error({message : 'Exception error', 
                  error, 
                  func: 'get-transactions-by-date', 
                  tags: 'transaction',
                  extras: {userId: user_id}});
    return res.status(400).send("Exception error"); */
  }
});

router.get('/iva', async (req, res) => {
  console.log(req.query)
  /*const user = req.ctx.user;
   if (!user) {
     return res.status(401).send({ message: 'Usuario no autenticado' });
   }*/
  const { user = 8, profile_id, start_date, end_date } = req.query;
  let creditos;
  let debitos;
  let aporteCreditos = 0;
  let aporteDebitos = 0;
  sDate = new Date(start_date)
  sDate.setMinutes(sDate.getMinutes() + sDate.getTimezoneOffset())
  eDate = new Date(end_date)
  eDate.setMinutes(eDate.getMinutes() + eDate.getTimezoneOffset())
  formattedEDate = date.format(eDate, 'YYYY-MM-DD');
  formattedSDate = date.format(sDate, 'YYYY-MM-DD');

  creditos = await prisma.revenue.findMany({
    where: {
      profile_id: parseInt(profile_id, 10),
      user_id: parseInt(user),
      trx_date: {
        gte: new Date(formattedSDate),
        lt: new Date(formattedEDate)
      },
      NOT: {
        iva_percentage: null
      }
    },
    select: {
      amount: true,
      iva_percentage: true,
    }
  })


  debitos = await prisma.expenses.findMany({
    where: {
      profile_id: parseInt(profile_id, 10),
      user_id: parseInt(user),
      trx_date: {
        gte: new Date(formattedSDate),
        lt: new Date(formattedEDate)
      },
      NOT: {
        iva_percentage: null
      }
    },
    select: {
      amount: true,
      iva_percentage: true,
    }
  })

  creditos.map((trx) => {
    aporteCreditos += (trx.amount - (trx.amount / (1 + (trx.iva_percentage / 100))))

  })

  debitos.map((trx) => {
    aporteDebitos += (trx.amount - (trx.amount / (1 + (trx.iva_percentage / 100))))
  })
  let ivaTotal = {
    ivatotal: (aporteCreditos.toFixed(3) - aporteDebitos.toFixed(3)).toFixed(3),
    aporteCreditos : aporteCreditos.toFixed(3),
    aporteDebitos : aporteDebitos.toFixed(3)
      
  }
  
    
 

  return res.status(200).send(ivaTotal);
});
router.get('/renta', async (req, res) => {
  
  /*const user = req.ctx.user;
   if (!user) {
     return res.status(401).send({ message: 'Usuario no autenticado' });
   }*/
  const { user = 8, profile_id, start_date, end_date } = req.query;
  let pagoRentaTotal;
  let exceso;
  let rentaBruta;
  let gastos;
  let rentaNeta;
  sDate = new Date(start_date)
  sDate.setMinutes(sDate.getMinutes() + sDate.getTimezoneOffset())
  eDate = new Date(end_date)
  eDate.setMinutes(eDate.getMinutes() + eDate.getTimezoneOffset())
  formattedEDate = date.format(eDate, 'YYYY-MM-DD');
  formattedSDate = date.format(sDate, 'YYYY-MM-DD');
  switch (parseInt(profile_id, 10)) {
    case 2:
      rentaBruta = await prisma.revenue.aggregate({
        _sum: {
          amount: true
        },
        where: {
          user_id: user,
          profile_id: 2,
          trx_date: {
            gte: new Date(formattedSDate),
            lt: new Date(formattedEDate)
          }
        }
      });
      gastos = await prisma.expenses.aggregate({
        _sum: {
          amount: true
        },
        where: {
          user_id: user,
          profile_id: 2,
          trx_date: {
            gte: new Date(formattedSDate),
            lt: new Date(formattedEDate)
          }
        }
      });

      rentaNeta = (parseFloat(rentaBruta._sum.amount) - parseFloat(gastos._sum.amount));
      if ((0 < rentaNeta) && (rentaNeta < 3742000)) {


        pagoRentaTotal = (rentaNeta * 0)
      } else if ((3742000 < rentaNeta) && (rentaNeta < 5589000)) {

        exceso = (rentaNeta - 3742000)
        pagoRentaTotal = (exceso * 0.10)
      } else if ((5589000 < rentaNeta) && (rentaNeta < 9322000)) {

        exceso = (rentaNeta - 5589000)
        pagoRentaTotal = (exceso * 0.15) + (5589000 * 0.10)
      } else if ((9322000 < rentaNeta) && (rentaNeta < 18683000)) {

        exceso = (rentaNeta - 9322000)
        pagoRentaTotal = (exceso * 0.20) + (5589000 * 0.10) + (9322000 * 0.15)
      } else if ((rentaNeta > 18683000)) {

        pagoRentaTotal = (rentaNeta * 0.25)
        console.log(pagoRentaTotal)
      }

      break;
    case 3:
      rentaBruta = await prisma.revenue.aggregate({
        _sum: {
          amount: true
        },
        where: {
          user_id: user,
          profile_id: 3,
          trx_date: {
            gte: new Date(formattedSDate),
            lt: new Date(formattedEDate)
          }
        }
      });
      gastos = await prisma.expenses.aggregate({
        _sum: {
          amount: true
        },
        where: {
          user_id: user,
          profile_id: 3,
          trx_date: {
            gte: new Date(formattedSDate),
            lt: new Date(formattedEDate)
          }
        }
      });
      let payroll = await prisma.payroll.aggregate({
        _sum: {
          amount: true
        },
        where: {
          user_id: user,
          profile_id: 3,
          trx_date: {
            gte: new Date(formattedSDate),
            lt: new Date(formattedEDate)
          }
        }
      });
      rentaNeta = (parseFloat(rentaBruta._sum.amount) - (parseFloat(gastos._sum.amount) + parseFloat(payroll._sum.amount)));

      if ((0 < rentaNeta) && (rentaNeta < 5157000)) {

        pagoRentaTotal = (rentaNeta * 0.05)
      } else if ((5157000 < rentaNeta) && (rentaNeta < 7737000)) {

        exceso = (rentaNeta - 5157000)
        pagoRentaTotal = (exceso * 0.10) + (5157000 * 0.05)
      } else if ((7737000 < rentaNeta) && (rentaNeta < 10315000)) {

        exceso = (rentaNeta - 7737000)
        pagoRentaTotal = (exceso * 0.15) + (7737000 * 0.10) + (5157000 * 0.05)
      } else if ((10315000 < rentaNeta) && (rentaNeta < 109337000)) {

        exceso = (rentaNeta - 10315000)
        pagoRentaTotal = (exceso * 0.20) + (10315000 * 0.15) + (7737000 * 0.10) + (5157000 * 0.05)
      } else if ((rentaNeta > 109337000)) {
        pagoRentaTotal = (rentaNeta * 0.30)
      }

      break;

    default:
      break;
  }

  let result = {
  
    rentaBruta: parseFloat(rentaBruta._sum.amount),
    gastos: parseFloat(gastos._sum.amount),
    Balance: rentaNeta,
    pagoRentaTotal: pagoRentaTotal
  }
  return res.status(200).send(result);
});

router.get('/ccss', async (req, res) => {
  
  /*const user = req.ctx.user;
  if (!user) {
    return res.status(401).send({ message: 'Usuario no autenticado' });
  }*/
  const { user = 8, profile_id, start_date, end_date } = req.query;
  sDate = new Date(start_date)
  sDate.setMinutes(sDate.getMinutes() + sDate.getTimezoneOffset())
  eDate = new Date(end_date)
  eDate.setMinutes(eDate.getMinutes() + eDate.getTimezoneOffset())
  formattedEDate = date.format(eDate, 'YYYY-MM-DD');
  formattedSDate = date.format(sDate, 'YYYY-MM-DD');
  let revenueTotal;
  let montoAPagar;
  let result;
  switch (parseInt(profile_id, 10)) {
    case 2:
      revenueTotal = await prisma.revenue.aggregate({
        _sum: {
          amount: true
        },
        where: {
          user_id: user,
          profile_id: 2,
          trx_date: {
            gte: new Date(formattedSDate),
            lt: new Date(formattedEDate)
          }
        }
      })
      const ingresoBruto = parseFloat(revenueTotal._sum.amount);

      if (ingresoBruto < 297044) {
        montoAPagar = (ingresoBruto * 0.672)
      } else if ((297045 <= ingresoBruto) && (ingresoBruto <= 639149)) {
        montoAPagar = (ingresoBruto * 0.965)
      } else if ((639150 <= ingresoBruto) && (ingresoBruto <= 1278298)) {
        montoAPagar = (ingresoBruto * 0.1344)
      } else if ((1278299 <= ingresoBruto) && (ingresoBruto <= 1917447)) {
        montoAPagar = (ingresoBruto * 0.1567)
      } else if ((19174480 <= ingresoBruto)) {
        montoAPagar = (ingresoBruto * 0.1878)
      }
      result = {
        profile: 3,montoAPagar: montoAPagar.toFixed(3)}
        
        
      

      return res.status(200).send(result);
    case 3:
      let payroll = await prisma.payroll.aggregate({
        _sum: {
          amount: true
        },
        where: {
          user_id: user,
          profile_id: 3,
          trx_date: {
            gte: new Date(formattedSDate),
            lt: new Date(formattedEDate)
          }
        }
      });
      
      const montoPatrono = parseFloat(payroll._sum.amount) * 0.265;
      const montoTrabajador =  parseFloat(payroll._sum.amount) * 0.105;
      montoAPagar = montoPatrono + montoTrabajador;
      
      result = {montoPatrono : montoPatrono.toFixed(3),
        montoTrabajador : montoTrabajador.toFixed(3),
        montoAPagar:  montoAPagar.toFixed(3)};
      

      return res.status(200).send(result);


    default:
      break;
  }
})


module.exports = router;


