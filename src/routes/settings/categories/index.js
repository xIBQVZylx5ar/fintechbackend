const express = require('express');
const prisma = require('../../../services/prisma');
const constants = require('../../../utils/constants');
const router = express.Router();

//Insert
router.post('/new', async (req, res) => {
    console.log('entraste_------------------');
    const { transaction_type, user_id = 8 } = req.body;
    let userID = user_id;

    if (!transaction_type) {
        return res.status(400).send("Parámetros insuficientes");
    }

    try {
        let data, context;
        switch (transaction_type) {
            case constants.TYPE_CATEGORIES_REVENUE_TRX:
                console.log('opcion 1------------------');
                if(!req.body.description || !req.body.profile_id ){    
                    return res.status(400).send("Parámetros insuficientes")
                }

                data = {
                    description: req.body.description,
                    profile_id: parseInt(req.body.profile_id),
                    user_id: userID
                }

                context = await prisma.origin.create({
                    data
                });

            break;

            case constants.TYPE_CATEGORIES_EXPENSES_TRX:
                  if(!req.body.description){
                    return res.status(401).send("Parámetros insuficientes")
                  }
                  data = {
                      user_id : userID,
                      description: req.body.description
                  }
                  context = await prisma.expcat.create({
                      data
                  });
            break;

            case constants.TYPE_CATEGORIES_DESTINATION_TRX:
                console.log('entramos');
                console.log(req.body);
                  if(!req.body.profile_id || !req.body.description){
                    return res.status(401).send("Parámetros insuficientes")
                  }
                  data = {
                    user_id: userID,
                    description: req.body.description,
                    profile_id: parseInt(req.body.profile_id)
                  }
                  context = await prisma.destination.create({
                    data
                  });
            break;

            case constants.TYPE_CATEGORIES_COLLABORATORS_TRX:
                if(!collaborator_id || !collab_name){
                    return res.status(401).send("Parámetros insuficientes")
                }
                data = {
                    collaborator_id: req.body.collaborator_id,
                    collab_name: req.body.collab_name,
                    user_id: userID
                }
                context = await prisma.collaborator.create({
                    data
                });
            break;

            default:
                console.log('Invalid Case');
            break;
        }

        if (context) {
            return res.status(200).send("Inserción exitosa");
          } else {
            return res.status(400).send("Inserción fallida");
          }
    
    }catch(error){
        /*logger.error({
            message: 'Exception error',
            error,
            func: 'new-transaction',
            tags: 'transaction',
            extras: { userId: user_id }
          });*/
          console.log(error);
        return res.status(400).send("Exception error");
    }
})

//Update
router.put('/update', async (req, res) => {

    console.log(req.body);
    const { transaction_type } = req.body;

    let data, context;
    try {
        switch (transaction_type) {
            case constants.TYPE_CATEGORIES_REVENUE_TRX:
                const { origin_id, description, profile, users} = req.body;
                
                if( !origin_id || !description || !profile || !users ){
                    return res.status(401).send("Parámetros insuficientes")
                }

                data = {
                    description: description,
                    profile_id: profile,
                    user_id: users
                }

                context = await prisma.origin.update({
                    where: {
                        origin_id: parseInt(origin_id)
                    },
                    data
                });
            break;

            case constants.TYPE_CATEGORIES_EXPENSES_TRX:
                const { id_type, descriptio} = req.body;
                if(!id_type || !descriptio){
                    return res.status(401).send("Parámetros insuficientes")
                }

                data = {
                    description: descriptio
                }
                context = await prisma.expenses_type.update({
                    where: {
                        id_type: parseInt(id_type)
                    },
                    data
                });
            break;

            case constants.TYPE_CATEGORIES_PAYMENT_METHODS_TRX:
                const {id_cx, descripti} = req.body;
                if(!id_cx || !descripti){
                    return res.status(401).send("Parámetros insuficientes")
                }

                data = {
                    description: descripti
                }

                context = await prisma.cx_type.update({
                    where: {
                        id_cx: parseInt(id_cx)
                    },
                    data
                });

            break;

            case constants.TYPE_CATEGORIES_COLLABORATORS_TRX:
                const {collaborator_id, collab_name, user} = req.body;
                if(!collaborator_id || !collab_name || !user){
                    return res.status(401).send("Parámetros insuficientes")
                }

                data = {
                    collab_name: collab_name,
                    user: user
                }

                context = await prisma.collaborator.update({
                    where: {
                        collaborator_id: parseInt(collaborator_id)
                    },
                    data
                });

            break;

        }
    }catch(err){
        return res.status(400).send(err);
    }
})


//Delete
router.delete('/remove', async (req, res) => {

    const { transactionId, transactionType } = req.query;

    if (!transactionId) {
        return res.status(401).send({ message: 'Parámetros insuficientes' });
    }

    let removing;

    switch (transactionType) {
        case constants.TYPE_CATEGORIES_REVENUE_TRX:
            removing = await prisma.origin.delete({
                where: {
                  origin_id: parseInt(transactionId, 10)
                }
              }).catch((err) => {
                return res.status(400).send(err);
              });
        break;

        case constants.TYPE_CATEGORIES_EXPENSES_TRX:
            removing = await prisma.expenses_type.delete({
                where: {
                  id_type: parseInt(transactionId, 10)
                }
              }).catch((err) => {
                return res.status(400).send(err);
              });
        break;

        case constants.TYPE_CATEGORIES_PAYMENT_METHODS_TRX:
            removing = await prisma.cx_type.delete({
                where: {
                  id_cx: parseInt(transactionId, 10)
                }
              }).catch((err) => {
                return res.status(400).send(err);
              });
        break;

        case constants.TYPE_CATEGORIES_COLLABORATORS_TRX:
            removing = await prisma.collaborator.delete({
                where: {
                  collaborator_id: parseInt(transactionId, 10)
                }
              }).catch((err) => {
                return res.status(400).send(err);
              });
        break
    }
})

router.get('/',async (req,res) => {
    const { transaction_type, profile_id, user_id = 8} = req.query;
    if (!transaction_type || !profile_id || !user_id) {
        return res.status(400).send("Parámetros insuficientes");
    }

    /*const report = [];
    let extype;
    let sqlReport;*/
    let transactions;
    switch (transaction_type) {
        case constants.TYPE_CATEGORIES_REVENUE_TRX:
            /*transactions = await prisma
            .$queryRaw(`SELECT o."description" FROM public.origin o WHERE o."profile_id" = ${parseInt(profile_id, 10)} `)
            .catch((err) => {
              return res.status(400).send(err);
            });*/
            transactions = await prisma.origin.findMany({
                where : {
                    user_id: user_id
                }
            })
            .catch((err) => {
              return res.status(400).send(err);
            });
        break;

        case constants.TYPE_CATEGORIES_EXPENSES_TRX:
            /*transactions = await prisma
            .$queryRaw(`SELECT e."description" FROM public.expenses_type e`)
            .catch((err) => {
              return res.status(400).send(err);
            });*/
            transactions = await prisma.expenses_type.findMany({
                where: {
                    
                }
            }).catch((err) => {
              return res.status(400).send(err);
            });
        break;

        case constants.TYPE_CATEGORIES_PAYMENT_METHODS_TRX:
            case constants.TYPE_CATEGORIES_EXPENSES_TRX:
            transactions = await prisma
            .$queryRaw(`SELECT c."description" FROM public.cx_type c`)
            .catch((err) => {
              return res.status(400).send(err);
            });
        break;

        case constants.TYPE_CATEGORIES_COLLABORATORS_TRX:
            transactions = await prisma
            .$queryRaw(`SELECT c."collab_name" FROM public.collaborator c WHERE c."user_id" = ${parseInt(user_id, 10)}`)
            .catch((err) => {
              return res.status(400).send(err);
            });
        break;
    }

    console.log(transactions);
    return res.status(200).send(categoryList);
});

module.exports = router;
