const express = require('express');
const jsonwebtoken = require('jsonwebtoken');
const auth = require('../../lib/auth');
const { JWT_SECRET } = require('../../config');

const router = express.Router();
const generateTokens = (id) => {
  return {
    token: jsonwebtoken.sign({ id }, JWT_SECRET, {
      expiresIn: '10d',
    }),
  };
};
router.post('/login', async (req, res) => {
  const { email, password } = req.body;
  const user = await auth.login(email, password).catch((err) => {
    console.error(err);
    return false;
  });
  if (!user) {
    return res
      .status(400)
      .send({ message: 'Usuario o contraseña incorrectos' });
  }
  return res.status(200).send({
    tokens: generateTokens(user.id),
    user,
  });
});

router.post('/register', async (req, res) => {
  const { email, password, name, last_name } = req.body;
  const exist = await auth.exist({ email }).catch(() => {
    return false;
  });
  if (exist) {
    return res
      .status(400)
      .send({ message: 'Este correo ya se encuentra en uso' });
  }
  const newUser = await auth
    .register({
      email,
      name: name.toUpperCase(),
      last_name: last_name.toUpperCase(),
      organization: 'bananacode',
      password,
      status: 'active',
      user_type: 'user',
    })
    .catch((err) => {
      return res.status(400).send({ message: err.msg });
    });

  return res.status(200).send({
    tokens: generateTokens(newUser.id),
    user: newUser,
  });
});
router.get('/me', (req, res) => {
  const ctx = req.ctx;
  if (!ctx.user) {
    return res.status(401).send('Usuario no autenticado');
  }
  return res.status(200).send(ctx.user);
});
module.exports = router;
