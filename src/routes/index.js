const express = require('express');
const auth = require('./auth');
const transactions = require('./transaction');
const settings = require('./settings');
const blog = require('./blog')
const comments = require('./comments')
const planilla = require('./planilla')

const router = express.Router();
router.use('/auth', auth);
router.use('/transaction', transactions);
router.use('/settings', settings);
router.use('/blog', blog); // RUTA DE BLOG
router.use('/comments', comments); // RUTA DE COMENTARIOS
router.use('/planilla', planilla); // RUTA DE PLANILLA

module.exports = router;
