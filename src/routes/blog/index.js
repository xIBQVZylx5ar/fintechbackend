const express = require('express');
const { comments } = require('../../services/prisma');
const prisma = require('../../services/prisma');
const logger = require('../../utils/logger/');
const router = express.Router();

// CREAR UN NUEVO POST BLOG
router.post('/new', async(req, res) => {
    const { content, user_id = 8 } = req.body;
    let userID = user_id;

    if(!content){
        return res.status(401).send('Parámetros insuficientes');
    }

    try{
        let blog;
        let data;

        data = {
            user_id: userID,
            content: content
        }

        blog = await prisma.blog_post.create({data})
        
        if(blog) {
            return res.status(200).send("Inserción exitosa");
        } else {
            return res.status(400).send("Inserción fallida");
        }
      
    } catch(error){
        return res.status(400).send(error)
    }
});

// ACTUALIZAR UN POST BLOG
router.put('/update', async(req, res) => {
    const { content, user_id = 8, postId=1} = req.body;

    let userID = user_id

    if(!content || !postId){
        res.status(401).send("Parámetros insuficientes")
    }

    try{
        let data;
        let blog;

        data = {
            user_id: userID,
            content: content,
        }

        blog = await prisma.blog_post.update({
            where:{
                post_id: parseInt(postId)
            },
            data
        })

        if(blog) {
            return res.status(200).send("Inserción exitosa");
        } else {
            return res.status(400).send("Inserción fallida");
        }
    }catch(error){
        return res.status(400).send(error)
    }
});

// ELIMINAR POR ID EL POST BLOG
router.delete('/remove', async (req, res) => {
    const { postId } = req.query;

    if(!postId){
        return res.status(401).send("Parámetros insuficientes");
    }

    try{
        let blog;

        blog = await prisma.blog_post.delete({
            where:{
                post_id: parseInt(postId)
            }
        }).catch(err => {
            return res.status(400).send(err)
        })

        if(blog){
            return res.status(200).send("Inserción exitosa")
        } else {
            return res.status(400).send("Inserción fallida")
        }
    } catch (error){
        return res.status(400).send(error)
    }
});

// LLAMAR A TODOS LOS POST BLOG
router.get('/',async (req,res) => {
    let blogAll;
    try{
        blogAll = await prisma.blog_post.findMany({
            orderBy: {
                created_at: "desc",
            },
            include: {
                comments: true
            }
        })
        return res.status(200).send(blogAll)
    } catch(error){
        return res.status(400).send(error)
    }
});

module.exports = router;