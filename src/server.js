const express = require('express');
const cors = require('cors');
const os = require('os');
const logger = require('./utils/logger');
const routes = require('./routes');
const context = require('./lib/context');
const { __PROD__ } = require('./config');

const hostname = os.hostname();
const app = express();

const PORT = process.env.PORT || 3000;

const start = async () => {
  app.use(cors());
  app.use(express.json({ limit: '100mb' }));
  // All routes
  app.use(context);
  app.use('/', routes);

  app.get('/health', (req, res) => {
    res.status(200).send(`Fintech -- is healthy -- from ${hostname}`);
  });

  await app.listen({ port: 3000 });
  console.log(`Apollo Server server ready at http://localhost:${PORT}`);
  console.log('Running on production = ' + __PROD__);
  logger.info({ message: 'Server started' });
};
module.exports = { start };
