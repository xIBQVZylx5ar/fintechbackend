/* eslint-disable no-console */
import { describe } from 'mocha';
import { expect } from 'chai';
import auth from '../src';
import { getGlobalUser } from '../src/utils';
import { generate } from 'generate-password';
import { v4 as uuid } from 'uuid';
const userData = getGlobalUser();

const reset = describe('Reset password Test', async () => {
	let correctCode = '';
	it('Should request reset url', async () => {
		correctCode = <string>(
			await auth.resetPasswordByUrl(userData.email).catch((err) => {
				console.error(err);
			})
		);
		expect(correctCode).to.be.string;
	}).timeout(4000);
	it('Should get incorrect email format error', async () => {
		const code = await auth
			.resetPasswordByUrl(
				generate({
					length: 10,
					symbols: false,
				})
			)
			.catch((err) => {
				return err.code;
			});
		expect(code).to.be.equal('018');
	}).timeout(1000);
	it('Should validate code successfully', async () => {
		const response = await auth.validateResetCode(correctCode).catch((err) => {
			return err.code;
		});
		expect(response).to.be.ok;
	}).timeout(2000);
	it('Should get incorrect code format', async () => {
		const code = await auth
			.validateResetCode(
				generate({
					length: 36,
				})
			)
			.catch((err) => {
				return err.code;
			});
		expect(code).to.be.equal('019');
	});
	it('Should get code not found', async () => {
		const code = await auth.validateResetCode(uuid()).catch((err) => {
			return err.code;
		});
		expect(code).to.be.equal('020');
	}).timeout(1000);
	it('Should get password password length error', async () => {
		const code = await auth
			.changePassword(
				correctCode,
				generate({
					numbers: true,
					length: 5,
				})
			)
			.catch((err) => {
				return err.code;
			});
		expect(code).to.be.equal('025');
	}).timeout(1000);
	it('Should get password password format error', async () => {
		const code = await auth
			.changePassword(
				correctCode,
				generate({
					numbers: false,
					length: 9,
					strict: true,
				})
			)
			.catch((err) => {
				return err.code;
			});
		expect(code).to.be.equal('026');
	});
	it('Should get code format error', async () => {
		const code = await auth
			.changePassword(
				generate({
					numbers: true,
					length: 9,
					strict: true,
				}),
				generate({
					numbers: true,
					length: 9,
					strict: true,
				})
			)
			.catch((err) => {
				return err.code;
			});
		expect(code).to.be.equal('022');
	});
	it('Should get code not found error', async () => {
		const code = await auth
			.changePassword(
				uuid(),
				generate({
					numbers: true,
					length: 9,
					strict: true,
				})
			)
			.catch((err) => {
				return err.code;
			});
		expect(code).to.be.equal('023');
	}).timeout(2000);
	it('Should change password successfully', async () => {
		const user = await auth
			.changePassword(correctCode, userData.password)
			.catch((err) => {
				return err.code;
			});
		expect(user).to.be.ok;
	}).timeout(5000);
});

export default reset;
