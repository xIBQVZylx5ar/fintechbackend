/* eslint-disable no-console */
import { describe } from 'mocha';
import { expect } from 'chai';
import auth from '../src';
import { getGlobalUser } from '../src/utils';
import { generate } from 'generate-password';

const userData = getGlobalUser();
const exist = describe('Exist Test', async () => {
	it('Should exist', async () => {
		const exist = await auth.exist({ email: userData.email }).catch((err) => {
			console.error(err);
		});
		expect(exist).to.be.true;
	}).timeout(2500);
	it('Should not exist', async () => {
		const gen = generate({
			length: 10,
			numbers: false,
			symbols: false,
			strict: true,
		});
		const exist = await auth
			.exist({ email: gen + '@' + gen + '.com' })
			.catch((err) => {
				console.error(err);
			});
		expect(exist).to.be.false;
	}).timeout(2500);
});

export default exist;
