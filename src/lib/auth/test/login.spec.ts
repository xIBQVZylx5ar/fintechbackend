/* eslint-disable no-console */
import { describe } from 'mocha';
import { generate } from 'generate-password';
import { expect } from 'chai';
import auth from '../src';
import { getGlobalUser } from '../src/utils';
const userData = getGlobalUser();

const login = describe('Login Test', async () => {
	it('Should successfully authenticate', async () => {
		const user = await auth
			.login(userData.email, userData.password)
			.catch((err) => {
				console.error(err);
			});
		expect(user).to.be.ok;
	}).timeout(2500);
	it('Should get incorrect password', async () => {
		const password = generate({
			length: 8,
			numbers: true,
			symbols: false,
			strict: true,
		});
		const code = await auth.login(userData.email, password).catch((err) => {
			return err.code; //This means that user login should fail by getting error code
		});
		expect(code).to.be.equal('003');
	}).timeout(2500);
	it('Should get email format error', async () => {
		const code = await auth
			.login(
				generate({ length: 8, numbers: true, symbols: false }),
				generate({ length: 8, numbers: true, symbols: false })
			)
			.catch((err) => {
				return err.code; //This means that user login should fail by getting error code
			});
		expect(code).to.be.equal('009');
	}).timeout(500);
	it('Should get length error', async () => {
		const code = await auth
			.login(
				userData.email,
				generate({ length: 7, numbers: true, symbols: false })
			)
			.catch((err) => {
				return err.code; //This means that user login should fail by getting error code
			});
		expect(code).to.be.equal('010');
	}).timeout(500);
	it('Should get password format error', async () => {
		const code = await auth
			.login(
				userData.email,
				generate({ length: 8, numbers: false, symbols: false })
			)
			.catch((err) => {
				return err.code; //This means that user login should fail by getting error code
			});
		expect(code).to.be.equal('011');
	}).timeout(500);
});

export default login;
