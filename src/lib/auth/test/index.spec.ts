import 'mocha';
// Files must be in order to ahieve the whole behaviour in the package
import register from './register.spec';
import resetPassword from './resetPassword.spec';
import exist from './exist.spec';
import login from './login.spec';
import deleted from './delete.spec';

resetPassword;
register;
exist;
login;
deleted;
