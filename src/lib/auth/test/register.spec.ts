/* eslint-disable no-console */
import { describe } from 'mocha';
import { expect } from 'chai';
import auth from '../src';
import { getGlobalUser } from '../src/utils';
import { generate } from 'generate-password';

const userData = getGlobalUser();

const register = describe('Register Test', async () => {
	it('Should successfully create user', async () => {
		const user = await auth.register(userData).catch((err) => {
			console.error(err);
		});
		expect(user).to.be.ok;
	}).timeout(6000);
	it('Should get unique email error', async () => {
		const code = await auth.register(userData).catch((err) => {
			return err.code;
		});
		expect(code).to.be.equal('012');
	}).timeout(2000);
	it('Should get email format error', async () => {
		const wrongUserData = { ...userData };
		wrongUserData.email = generate({
			length: 5,
			numbers: true,
			symbols: false,
		});
		const code = await auth.register(wrongUserData).catch((err) => {
			return err.code;
		});
		expect(code).to.be.equal('004');
	}).timeout(1000);
	it('Should get password length error', async () => {
		const wrongUserData = { ...userData };
		wrongUserData.password = generate({
			length: 5,
			numbers: true,
			symbols: false,
		});
		const code = await auth.register(wrongUserData).catch((err) => {
			return err.code;
		});
		expect(code).to.be.equal('005');
	}).timeout(1000);
	it('Should get password format error', async () => {
		const wrongUserData = { ...userData };
		wrongUserData.password = generate({
			length: 8,
			numbers: false,
			symbols: false,
			strict: true,
		});
		const code = await auth.register(wrongUserData).catch((err) => {
			return err.code;
		});
		expect(code).to.be.equal('006');
	}).timeout(1000);
	it('Should get name length error', async () => {
		const wrongUserData = { ...userData };
		wrongUserData.name = generate({
			length: 31,
			numbers: false,
			symbols: false,
		});
		const code = await auth.register(wrongUserData).catch((err) => {
			return err.code;
		});
		expect(code).to.be.equal('007');
	}).timeout(1000);
	it('Should get last name length error', async () => {
		const wrongUserData = { ...userData };
		wrongUserData.last_name = generate({
			length: 31,
			numbers: false,
			symbols: false,
		});
		const code = await auth.register(wrongUserData).catch((err) => {
			return err.code;
		});
		expect(code).to.be.equal('008');
	}).timeout(1000);
});

export default register;
