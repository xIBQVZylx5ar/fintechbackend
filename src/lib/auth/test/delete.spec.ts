/* eslint-disable no-console */
import { describe } from 'mocha';
import { expect } from 'chai';
import auth from '../src';
import { getGlobalUser } from '../src/utils';
const userData = getGlobalUser();

const deleted = describe('Delete Test', async () => {
	it('Should successfully delete user', async () => {
		const deletedUser = await auth
			.deleteUser({ email: userData.email })
			.catch((err) => {
				console.error(err);
			});
		expect(deletedUser).to.be.ok;
	}).timeout(3500);
	it('Should get delete error', async () => {
		const code = await auth
			.deleteUser({ email: userData.email })
			.catch((err) => {
				return err.code;
			});
		expect(code).to.be.equal('P2025'); //quick fix get prisma delete error
	}).timeout(3500);
});

export default deleted;
