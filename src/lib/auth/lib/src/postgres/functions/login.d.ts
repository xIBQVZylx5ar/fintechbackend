import { User } from '../../@types';
declare const login: (email: string, password: string) => Promise<User | undefined>;
export default login;
