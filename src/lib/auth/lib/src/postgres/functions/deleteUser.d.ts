import { DeleteOptions, User } from '../../@types';
declare const deleteUser: ({ id, email }: DeleteOptions) => Promise<User>;
export default deleteUser;
