"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const AuthError_1 = __importDefault(require("../../classes/AuthError"));
const uuid = __importStar(require("uuid"));
const utils_1 = require("../../utils");
const validateResetCode = (code) => __awaiter(void 0, void 0, void 0, function* () {
    if (!uuid.validate(code))
        throw new AuthError_1.default('Incorrect code format', '019');
    const recovery = yield utils_1.prisma.code
        .findUnique({
        where: {
            code,
        },
    })
        .catch((err) => {
        throw err;
    });
    if (!recovery)
        throw new AuthError_1.default('Code not found', '020');
    const now = new Date().getTime();
    const diff = Math.floor(now - recovery.created_at.getTime());
    const minutes = Math.round(diff / 1000 / 60);
    if (minutes > 5)
        throw new AuthError_1.default('Code has expired', '021');
    return true;
});
exports.default = validateResetCode;
//# sourceMappingURL=validateResetCode.js.map