"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const validator_1 = __importDefault(require("validator"));
const prisma_1 = __importDefault(require("../../utils/prisma"));
const utils_1 = require("../../utils");
const AuthError_1 = __importDefault(require("../../classes/AuthError"));
const login = (email, password) => __awaiter(void 0, void 0, void 0, function* () {
    if (!validator_1.default.isEmail(email))
        throw new AuthError_1.default('Incorrect email format', '009');
    if (password.length < 8)
        throw new AuthError_1.default('Password must be at least 8 characters', '010');
    if (validator_1.default.isAlpha(password) || validator_1.default.isNumeric(password))
        throw new AuthError_1.default('Password must contain numbers and letters', '011');
    const user = yield prisma_1.default.user
        .findUnique({
        where: {
            email: email.toLowerCase(),
        },
    })
        .catch((err) => {
        throw err;
    });
    if (!user)
        return;
    if (!(yield utils_1.hasher.comparePassword(password, user.password)))
        throw new AuthError_1.default(`Incorrect Password`, '003');
    return utils_1.formatUser(user);
});
exports.default = login;
//# sourceMappingURL=login.js.map