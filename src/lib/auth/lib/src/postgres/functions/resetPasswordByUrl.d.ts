import { EmailExtraOptions } from 'src/@types';
declare const resetPasswordByUrl: (email: string, id?: number | undefined, options?: EmailExtraOptions | undefined) => Promise<string>;
export default resetPasswordByUrl;
