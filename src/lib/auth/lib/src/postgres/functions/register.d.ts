import { RegisterOptions, User, RegisterExtraOptions } from '../../@types';
declare const register: ({ birth_date, organization, status, user_type, password, email, last_name, name, }: RegisterOptions, options?: RegisterExtraOptions | undefined) => Promise<User>;
export default register;
