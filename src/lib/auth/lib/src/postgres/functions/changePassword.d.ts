import { User } from '../../@types';
declare const changePassword: (code: string, password: string) => Promise<User>;
export default changePassword;
