declare const validateResetCode: (code: string) => Promise<boolean>;
export default validateResetCode;
