"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const prisma_1 = __importDefault(require("../../utils/prisma"));
const validator_1 = __importDefault(require("validator"));
const utils_1 = require("../../utils");
const AuthError_1 = __importDefault(require("../../classes/AuthError"));
const utils_2 = require("../../utils");
const { USE_MAIL, USE_MAILGUN, SMTP, SENDER_NAME, SENDER_EMAIL, DOMAIN, LOGIN_PATH, } = utils_2.getEnviromentals();
const register = ({ birth_date, organization, status, user_type, password, email, last_name, name, }, options) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c, _d, _e, _f, _g;
    if (name.length > 30)
        throw new AuthError_1.default('Name max length is 30 characters', '007');
    if (last_name.length > 30)
        throw new AuthError_1.default('Last name max length is 30 characters', '008');
    if (!validator_1.default.isEmail(email))
        throw new AuthError_1.default('Incorrect email format', '004');
    if (password.length < 8)
        throw new AuthError_1.default('Password must be at least 8 characters', '005');
    if (validator_1.default.isAlpha(password) || validator_1.default.isNumeric(password))
        throw new AuthError_1.default('Password must contain numbers and letters', '006');
    const birth = birth_date ? new Date(birth_date) : null;
    const hashedPassword = yield utils_1.hasher.hashPassword(password);
    const user = yield prisma_1.default.user
        .create({
        data: {
            birth_date: birth,
            organization: organization.toLowerCase(),
            status,
            user_type: user_type.toLowerCase(),
            password: hashedPassword,
            email,
            last_name,
            name,
        },
    })
        .catch((err) => {
        if ((err === null || err === void 0 ? void 0 : err.code) === 'P2002') {
            throw new AuthError_1.default('Email has to be unique', '012');
        }
        throw err;
    });
    const formattedUser = utils_1.formatUser(user);
    if (USE_MAIL) {
        const link = `https://${DOMAIN}/${LOGIN_PATH}`;
        const from = ((_a = options === null || options === void 0 ? void 0 : options.email) === null || _a === void 0 ? void 0 : _a.from)
            ? options.email.from
            : `${SENDER_NAME} <${SENDER_EMAIL}>`;
        const subject = ((_b = options === null || options === void 0 ? void 0 : options.email) === null || _b === void 0 ? void 0 : _b.subject)
            ? options.email.subject
            : `Bienvenido`;
        const variables = ((_d = (_c = options === null || options === void 0 ? void 0 : options.email) === null || _c === void 0 ? void 0 : _c.template) === null || _d === void 0 ? void 0 : _d.variables)
            ? options.email.template.variables
            : {
                title: 'Bienvenido',
                body: 'Te damos la bienvenida a la paltaforma',
                footer: 'Gracias por creer en nosotros',
                callToAction: 'Inicar sesion',
                link,
            };
        const text = ((_e = options === null || options === void 0 ? void 0 : options.email) === null || _e === void 0 ? void 0 : _e.text)
            ? options.email.text
            : `Hola ${user.name} te damos la bienvenida a la plataforma inicia sesión ${link}`;
        const templateName = ((_g = (_f = options === null || options === void 0 ? void 0 : options.email) === null || _f === void 0 ? void 0 : _f.template) === null || _g === void 0 ? void 0 : _g.name)
            ? options.email.template.name
            : 'welcome';
        if (USE_MAILGUN) {
            utils_1.mailer
                .sendTemplate({
                from,
                to: [formattedUser.email],
                subject: subject,
                variables: variables,
                template: templateName,
            })
                .then()
                .catch((err) => console.error(err));
        }
        else if (SMTP) {
            utils_1.mailer
                .sendSimple({
                from: from,
                to: [formattedUser.email],
                subject: subject,
                text: text,
            })
                .then()
                .catch((err) => console.error(err));
        }
    }
    return formattedUser;
});
exports.default = register;
//# sourceMappingURL=register.js.map