"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const AuthError_1 = __importDefault(require("../../classes/AuthError"));
const utils_1 = require("../../utils");
const uuid_1 = require("uuid");
const validator_1 = __importDefault(require("validator"));
const { USE_MAILGUN, SMTP, DOMAIN, USE_MAIL, PASSWORD_PATH, } = utils_1.getEnviromentals();
const resetPasswordByUrl = (email, id, options) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    if (!validator_1.default.isEmail(email))
        throw new AuthError_1.default('Incorrect email format', '018');
    if (!USE_MAIL)
        throw new AuthError_1.default('Mail configuration is not set', '016');
    let user_id;
    if (id) {
        user_id = id;
    }
    else {
        const user = yield utils_1.prisma.user
            .findUnique({ where: { email: email } })
            .catch((err) => {
            throw err;
        });
        user_id = (user === null || user === void 0 ? void 0 : user.id) || 0;
    }
    const existCode = yield utils_1.prisma.code.findUnique({
        where: {
            user_id,
        },
    });
    const code = uuid_1.v4();
    if (existCode) {
        if (existCode.requests < 5) {
            yield utils_1.prisma.code
                .update({
                where: { user_id },
                data: {
                    created_at: new Date(Date.now()),
                    code,
                    requests: existCode.requests + 1,
                },
            })
                .catch((err) => {
                throw err;
            });
        }
        else {
            const now = new Date().getTime();
            const diff = Math.floor(now - existCode.created_at.getTime());
            const minutes = Math.round(diff / 1000 / 60);
            if (minutes < 60) {
                throw new AuthError_1.default('Exceeded maximum requests', '017');
            }
            else {
                yield utils_1.prisma.code
                    .update({
                    where: { user_id },
                    data: {
                        code,
                        requests: 1,
                    },
                })
                    .catch((err) => {
                    throw err;
                });
            }
        }
    }
    else {
        yield utils_1.prisma.code.create({
            data: {
                code,
                created_at: new Date(Date.now()),
                requests: 1,
                user_id,
            },
        });
    }
    const link = `https://${DOMAIN}/${PASSWORD_PATH}?code=${code}`;
    const from = (options === null || options === void 0 ? void 0 : options.from) ? options.from : `NO REPLY <no-reply@${DOMAIN}>`;
    const subject = (options === null || options === void 0 ? void 0 : options.subject) ? options.subject : `Recuperación`;
    const variables = ((_a = options === null || options === void 0 ? void 0 : options.template) === null || _a === void 0 ? void 0 : _a.variables)
        ? options.template.variables
        : {
            title: 'Contraseña',
            body: 'Ha solicitado un cambio de contraseña',
            footer: 'Link válido por 5 minutos',
            callToAction: 'Cambiar contraseña',
            link,
        };
    const text = (options === null || options === void 0 ? void 0 : options.text)
        ? options.text
        : `Cambio de contraseña ${link} \n Expira en 5 minutos`;
    const templateName = ((_b = options === null || options === void 0 ? void 0 : options.template) === null || _b === void 0 ? void 0 : _b.name)
        ? options.template.name
        : 'welcome';
    if (USE_MAILGUN) {
        utils_1.mailer
            .sendTemplate({
            from,
            to: [email],
            subject: subject,
            variables: variables,
            template: templateName,
        })
            .then()
            .catch((err) => console.error(err));
    }
    else if (SMTP) {
        utils_1.mailer
            .sendSimple({
            from: from,
            to: [email],
            subject: subject,
            text: text,
        })
            .then()
            .catch((err) => console.error(err));
    }
    return code;
});
exports.default = resetPasswordByUrl;
//# sourceMappingURL=resetPasswordByUrl.js.map