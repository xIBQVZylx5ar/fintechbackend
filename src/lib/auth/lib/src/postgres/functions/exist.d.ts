import { ExistOptions } from '../../@types';
declare const exist: ({ id, email }: ExistOptions) => Promise<boolean>;
export default exist;
