"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const login_1 = __importDefault(require("./functions/login"));
const exist_1 = __importDefault(require("./functions/exist"));
const register_1 = __importDefault(require("./functions/register"));
const deleteUser_1 = __importDefault(require("./functions/deleteUser"));
const resetPasswordByUrl_1 = __importDefault(require("./functions/resetPasswordByUrl"));
const validateResetCode_1 = __importDefault(require("./functions/validateResetCode"));
const changePassword_1 = __importDefault(require("./functions/changePassword"));
exports.default = {
    login: login_1.default,
    exist: exist_1.default,
    register: register_1.default,
    deleteUser: deleteUser_1.default,
    resetPasswordByUrl: resetPasswordByUrl_1.default,
    validateResetCode: validateResetCode_1.default,
    changePassword: changePassword_1.default,
};
//# sourceMappingURL=index.js.map