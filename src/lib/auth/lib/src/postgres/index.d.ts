declare const _default: {
    login: (email: string, password: string) => Promise<import("../@types").User | undefined>;
    exist: ({ id, email }: import("../@types").ExistOptions) => Promise<boolean>;
    register: ({ birth_date, organization, status, user_type, password, email, last_name, name, }: import("../@types").RegisterOptions, options?: import("../@types").RegisterExtraOptions | undefined) => Promise<import("../@types").User>;
    deleteUser: ({ id, email }: import("../@types").DeleteOptions) => Promise<import("../@types").User>;
    resetPasswordByUrl: (email: string, id?: number | undefined, options?: import("../@types").EmailExtraOptions | undefined) => Promise<string>;
    validateResetCode: (code: string) => Promise<boolean>;
    changePassword: (code: string, password: string) => Promise<import("../@types").User>;
};
export default _default;
