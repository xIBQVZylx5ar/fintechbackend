"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const AuthError_1 = __importDefault(require("./classes/AuthError"));
const postgres_1 = __importDefault(require("./postgres"));
const utils_1 = require("./utils");
const { ENGINE } = utils_1.getEnviromentals();
const auth = () => {
    if (!ENGINE) {
        throw new AuthError_1.default(`Database engine should be specified check the configuration path`, '001');
    }
    const engine = ENGINE || '';
    if (engine === 'postgres' || engine === 'mysql')
        return postgres_1.default;
    else if (engine === 'mongodb')
        return postgres_1.default;
    else
        throw new AuthError_1.default(`Correct database engine should be specified check  configuration path`, '002');
};
module.exports = auth();
//# sourceMappingURL=index.js.map