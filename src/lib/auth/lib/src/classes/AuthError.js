"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AuthError extends Error {
    constructor(message, code) {
        const log = '\n•----------------------•\n' +
            message +
            '\n' +
            'You can check this error into the documentation ' +
            'Error code: ' +
            code +
            '\n' +
            'https://github.com/bananacode-co/auth/blob/master/README.md' +
            '\n•----------------------•\n';
        ('\n');
        super(log);
        this.code = code;
        this.msg = message;
        Object.setPrototypeOf(this, AuthError.prototype);
    }
}
exports.default = AuthError;
//# sourceMappingURL=AuthError.js.map