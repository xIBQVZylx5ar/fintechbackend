declare class AuthError extends Error {
    code: string;
    msg: string;
    constructor(message: string, code: string);
}
export default AuthError;
