"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const env = process.env.NODE_ENV || 'developement';
const saltrounds = env === 'development' ? 5 : 10;
const hashPassword = (password) => {
    return bcrypt_1.default.hash(password, saltrounds);
};
const comparePassword = (password, hashedPassword) => {
    return bcrypt_1.default.compare(password, hashedPassword);
};
exports.default = { hashPassword, comparePassword };
//# sourceMappingURL=hash.js.map