import { SendSimpleOptions, TemplateOptions } from '../@types/index';
import mailgunjs from 'mailgun-js';
declare const _default: {
    sendSimple: ({ from, to, subject, text, }: SendSimpleOptions) => Promise<any>;
    sendTemplate: ({ to, template, from, subject, variables, }: TemplateOptions) => Promise<mailgunjs.messages.SendResponse | undefined>;
};
export default _default;
