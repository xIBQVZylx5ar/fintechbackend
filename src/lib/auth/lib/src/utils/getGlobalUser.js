"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_json_1 = require("../config/config.json");
const env = process.env.NODE_ENV || 'development';
const userConfig = {
    name: config_json_1.user.name,
    birth_date: config_json_1.user.birth_date,
    organization: config_json_1.user.organization,
    status: config_json_1.user.status,
    user_type: config_json_1.user.user_type,
    password: config_json_1.user.password,
    email: config_json_1.user.email,
    last_name: config_json_1.user.last_name,
};
const getUserConfig = () => {
    if (env === 'development') {
        return userConfig;
    }
    return {
        birth_date: process.env.USER_BIRTH_DATE
            ? Number.parseInt(process.env.USER_BIRTH_DATE)
            : config_json_1.user.birth_date,
        organization: process.env.USER_ORGANIZATION || config_json_1.user.organization,
        status: process.env.USER_STATUS || config_json_1.user.status,
        user_type: process.env.USER_TYPE || config_json_1.user.user_type,
        password: process.env.USER_PASSWORD || config_json_1.user.password,
        email: process.env.USER_EMAIL || config_json_1.user.email,
        last_name: process.env.USER_LAST_NAME || config_json_1.user.last_name,
        name: process.env.USER_NAME || config_json_1.user.name,
    };
};
exports.default = getUserConfig;
//# sourceMappingURL=getGlobalUser.js.map