"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const formatUser = (user) => {
    return {
        id: user.id,
        name: user.name,
        last_name: user.last_name,
        birth_date: user.birth_date,
        status: user.status,
        created_at: user.created_at,
        email: user.email,
        organization: user.organization,
        password: user.password,
        user_type: user.user_type,
    };
};
exports.default = formatUser;
//# sourceMappingURL=formatUser.js.map