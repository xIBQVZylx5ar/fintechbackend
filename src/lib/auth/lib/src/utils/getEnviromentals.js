"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = require("dotenv");
const AuthError_1 = __importDefault(require("../classes/AuthError"));
let envPath = __dirname + '/../../../prisma/.env';
const getEnviromentals = () => {
    var _a, _b;
    let dotConfig = dotenv_1.config({
        path: envPath,
    });
    if (!((_a = dotConfig.parsed) === null || _a === void 0 ? void 0 : _a.USE_MAIL)) {
        envPath = __dirname + '/../../prisma/.env';
        dotConfig = dotenv_1.config({
            path: envPath,
        });
        if (!((_b = dotConfig.parsed) === null || _b === void 0 ? void 0 : _b.USE_MAIL)) {
            throw new AuthError_1.default(`Specification of SMPT should be at least false ` + envPath, '015');
        }
    }
    return {
        USE_MAIL: process.env.USE_MAIL === 'true' || dotConfig.parsed.USE_MAIL === 'true',
        MAILGUN_API_KEY: process.env.MAILGUN_API_KEY || dotConfig.parsed.MAILGUN_API_KEY,
        MAILGUN_DOMAIN: process.env.MAILGUN_DOMAIN || dotConfig.parsed.MAILGUN_DOMAIN,
        USE_MAILGUN: process.env.USE_MAILGUN === 'true' ||
            dotConfig.parsed.USE_MAILGUN === 'true',
        SMTP: process.env.SMTP === 'true' || dotConfig.parsed.SMTP === 'true',
        SMTP_URL: process.env.SMTP_URL || dotConfig.parsed.SMTP_URL,
        SMTP_SECURE: process.env.SMTP_SECURE === 'true' ||
            dotConfig.parsed.SMTP_SECURE === 'true',
        NODE_ENV: process.env.NODE_ENV || 'ddevelopment',
        ENGINE: process.env.ENGINE || dotConfig.parsed.ENGINE,
        DATABASE_URL: process.env.DATABASE_URL || dotConfig.parsed.DATABASE_URL,
        SENDER_EMAIL: process.env.SENDER_EMAIL || dotConfig.parsed.SENDER_EMAIL
            ? dotConfig.parsed.SENDER_EMAIL
            : '',
        SENDER_NAME: process.env.SENDER_NAME || dotConfig.parsed.SENDER_NAME
            ? dotConfig.parsed.SENDER_NAME
            : '',
        DOMAIN: process.env.DOMAIN || dotConfig.parsed.DOMAIN
            ? dotConfig.parsed.DOMAIN
            : 'notset.com',
        PASSWORD_PATH: process.env.PASSWORD_PATH || dotConfig.parsed.PASSWORD_PATH
            ? dotConfig.parsed.PASSWORD_PATH
            : 'recovery',
        LOGIN_PATH: process.env.LOGIN_PATH || dotConfig.parsed.LOGIN_PATH
            ? dotConfig.parsed.LOGIN_PATH
            : 'login',
    };
};
exports.default = getEnviromentals;
//# sourceMappingURL=getEnviromentals.js.map