import { user } from '@prisma/client';
import { User } from '../@types';
declare const formatUser: (user: user) => User;
export default formatUser;
