"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const nodemailer_1 = __importDefault(require("nodemailer"));
const validator_1 = __importDefault(require("validator"));
const AuthError_1 = __importDefault(require("../classes/AuthError"));
const getEnviromentals_1 = __importDefault(require("./getEnviromentals"));
const mailgun_js_1 = __importDefault(require("mailgun-js"));
const { SMTP, SMTP_URL, SMTP_SECURE, MAILGUN_API_KEY, MAILGUN_DOMAIN, } = getEnviromentals_1.default();
const mailgun = mailgun_js_1.default({
    apiKey: MAILGUN_API_KEY,
    domain: MAILGUN_DOMAIN,
});
const getTransport = () => {
    let transporter;
    try {
        transporter = nodemailer_1.default.createTransport(SMTP_URL === null || SMTP_URL === void 0 ? void 0 : SMTP_URL.toString(), {
            secure: SMTP_SECURE,
        });
    }
    catch (error) {
        transporter = undefined;
    }
    return transporter;
};
const sendSimple = ({ from, to, subject, text, }) => __awaiter(void 0, void 0, void 0, function* () {
    const transporter = getTransport();
    if (!SMTP || !transporter)
        return;
    to.forEach((recipient) => {
        if (!validator_1.default.isEmail(recipient)) {
            throw new AuthError_1.default('One of the emails specified has an incorrect format', '014');
        }
    });
    const recipients = to.join(',');
    const info = yield transporter
        .sendMail({
        from: from,
        to: recipients,
        subject,
        text,
    })
        .catch((err) => {
        throw err;
    });
    return info;
});
const sendTemplate = ({ to, template, from, subject, variables, }) => __awaiter(void 0, void 0, void 0, function* () {
    to.forEach((recipient) => {
        if (!validator_1.default.isEmail(recipient)) {
            throw new AuthError_1.default('One of the emails specified has an incorrect format', '014');
        }
    });
    const recipients = to.join(',');
    const data = {
        from: from,
        to: recipients,
        subject,
        template,
        variables,
        'h:X-Mailgun-Variables': JSON.stringify(variables),
    };
    const response = yield mailgun.messages().send(data, (error, body) => {
        if (error) {
            console.error(error);
            return;
        }
        else {
            return body;
        }
    });
    return response;
});
exports.default = {
    sendSimple,
    sendTemplate,
};
//# sourceMappingURL=mailer.js.map