"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.closeConnection = void 0;
const client_1 = require("@prisma/client");
const prisma = new client_1.PrismaClient({});
const closeConnection = () => {
    prisma.$disconnect();
};
exports.closeConnection = closeConnection;
exports.default = prisma;
//# sourceMappingURL=prisma.js.map