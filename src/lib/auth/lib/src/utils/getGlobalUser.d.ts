import { UserStatus } from 'src/@types';
export interface UserData {
    birth_date: number;
    organization: string;
    status: UserStatus;
    user_type: string;
    password: string;
    email: string;
    last_name: string;
    name: string;
}
declare const getUserConfig: () => UserData;
export default getUserConfig;
