"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = require("dotenv");
const inquirer_1 = require("inquirer");
const fs_1 = require("fs");
const env = process.env.NODE_ENV || 'development';
const CI = process.env.CI || false;
const __PROD__ = env === 'production';
const STATIC = {
    database: 'Select your database engine',
    uri: (value) => `Write your ${value} connection string`,
    mongodb: 'mongodb://[username:password@]host1[:port1][,...hostN[:portN]][/[defaultauthdb][?options]]',
    postgres: 'postgresql://<username>:<password>@<hostname>:<port>/<database>',
    mysql: 'mysql://USER:PASSWORD@HOST:PORT/DATABASE',
    smtp: 'smtps://<address>:<password>@<domain.com>',
    secure: 'Select smtp configuration',
    useMail: 'Do you want to use email notifications?',
    use: 'Select email sender service?',
};
const configure = () => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c;
    if (CI || __PROD__) {
        fs_1.writeFileSync('prisma/.env', `DATABASE_URL=${process.env.DATABASE_URL}
ENGINE=${process.env.ENGINE}
USE_MAIL=${process.env.USE_MAIL}
USE_MAILGUN=${process.env.USE_MAILGUN}
MAILGUN_API_KEY=${process.env.MAILGUN_API_KEY}
MAILGUN_DOMAIN=${process.env.MAILGUN_DOMAIN}
SMTP=${process.env.SMTP}
SMTP_URL=${process.env.SMTP_URL}
SMTP_SECURE=${process.env.SMTP_SECURE}
SENDER_NAME=${process.env.SENDER_NAME}
SENDER_EMAIL=${process.env.SENDER_EMAIL}
DOMAIN=${process.env.DOMAIN}
PASSWORD_PATH=${process.env.PASSWORD_PATH}
LOGIN_PATH=${process.env.LOGIN_PATH}
	`);
        if (((_a = process.env.ENGINE) === null || _a === void 0 ? void 0 : _a.toLowerCase()) === 'postgresql' ||
            ((_b = process.env.ENGINE) === null || _b === void 0 ? void 0 : _b.toLowerCase()) === 'mysql') {
            fs_1.readFile('prisma/schema.prisma', 'utf8', function (err, data) {
                if (err) {
                    return console.log(err);
                }
                const result = data.replace(/postgresql/g, process.env.ENGINE || 'postgresql');
                fs_1.writeFileSync('prisma/schema.prisma', result, 'utf8');
            });
        }
    }
    else {
        const enviroment = dotenv_1.config({
            path: process.cwd() + '/prisma/.env',
        });
        if (!enviroment.error) {
            if ((_c = enviroment.parsed) === null || _c === void 0 ? void 0 : _c.DATABASE_URL) {
                return console.log('Running configuration from .env');
            }
        }
        const database = yield inquirer_1.prompt([
            {
                name: 'engine',
                message: STATIC.database,
                type: 'list',
                choices: [
                    { name: 'Postgres', value: 'postgres' },
                    { name: 'Mysql', value: 'mysql' },
                    { name: 'Mongodb', value: 'mongodb' },
                ],
            },
        ]);
        const uri = yield inquirer_1.prompt([
            {
                name: 'uri',
                message: STATIC.uri(database.engine),
                default: STATIC[database.engine],
                type: 'input',
            },
        ]);
        const connection = uri.uri === '' ? STATIC[database.engine] : uri.uri;
        const useMail = yield inquirer_1.prompt([
            {
                name: 'use',
                message: STATIC.useMail,
                type: 'list',
                choices: [
                    { name: 'Yes', value: true },
                    { name: 'No', value: false },
                ],
            },
        ]);
        const domain = yield inquirer_1.prompt([
            {
                name: 'domain',
                message: 'Write your domain name',
                default: 'bananacode.co',
                type: 'input',
            },
        ]);
        const paths = yield inquirer_1.prompt([
            {
                name: 'recovery',
                message: 'Write your password recovery route path name ',
                default: `recovery`,
            },
            {
                name: 'login',
                message: 'Write your login route path name ',
                default: `login`,
            },
        ]);
        let mailgun = {
            key: undefined,
            domain: undefined,
        };
        let smtp = { smtp: undefined };
        let secure = { secure: undefined };
        let use = { use: undefined };
        let sender = {
            name: undefined,
            email: undefined,
        };
        if (useMail.use) {
            use = yield inquirer_1.prompt([
                {
                    name: 'use',
                    message: STATIC.use,
                    type: 'list',
                    choices: [
                        { name: 'Mailgun Templates', value: 'mailgun' },
                        { name: 'SMTP Provider', value: 'smtp' },
                    ],
                },
            ]);
            sender = yield inquirer_1.prompt([
                {
                    name: 'name',
                    message: 'Write the name you want to send emails from',
                    default: 'John Doe',
                    type: 'input',
                },
                {
                    name: 'email',
                    message: 'Write the email you want to send emails from',
                    default: 'jdoe@bananacode.co',
                    type: 'input',
                },
            ]);
        }
        if (use.use === 'smtp') {
            smtp = yield inquirer_1.prompt([
                {
                    name: 'smtp',
                    message: 'Write your smtp connection string',
                    default: STATIC.smtp,
                    type: 'input',
                },
            ]);
            secure = yield inquirer_1.prompt([
                {
                    name: 'secure',
                    message: STATIC.secure,
                    type: 'list',
                    choices: [
                        { name: 'Secure', value: true },
                        { name: 'Not secure', value: false },
                    ],
                },
            ]);
        }
        else if (use.use === 'mailgun') {
            mailgun = yield inquirer_1.prompt([
                {
                    name: 'key',
                    message: 'Write your mailgun API_KEY',
                    default: '...',
                    type: 'input',
                },
                {
                    name: 'domain',
                    message: 'Write your mailgun DOMAIN',
                    default: '...',
                    type: 'input',
                },
            ]);
        }
        fs_1.writeFileSync('prisma/.env', `DATABASE_URL=${connection}
ENGINE=${database.engine}
USE_MAIL=${useMail.use}
USE_MAILGUN=${use.use === 'mailgun'}
MAILGUN_API_KEY=${mailgun.key}
MAILGUN_DOMAIN=${mailgun.domain}
SMTP=${use.use === 'smtp'}
SMTP_URL=${smtp.smtp}
SMTP_SECURE=${secure.secure}
SENDER_NAME=${sender.name}
SENDER_EMAIL=${sender.email}
DOMAIN=${domain.domain}
PASSWORD_PATH=${paths.recovery}
LOGIN_PATH=${paths.login}
	`);
        if (database.engine.toLowerCase() === 'postgresql' ||
            database.engine.toLowerCase() === 'mysql') {
            fs_1.readFile('prisma/schema.prisma', 'utf8', function (err, data) {
                if (err) {
                    return console.log(err);
                }
                const result = data.replace(/postgresql/g, database.engine || 'postgresql');
                fs_1.writeFileSync('prisma/schema.prisma', result, 'utf8');
            });
        }
    }
});
configure();
//# sourceMappingURL=configure.js.map