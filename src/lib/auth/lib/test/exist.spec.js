"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mocha_1 = require("mocha");
const chai_1 = require("chai");
const src_1 = __importDefault(require("../src"));
const utils_1 = require("../src/utils");
const generate_password_1 = require("generate-password");
const userData = utils_1.getGlobalUser();
const exist = mocha_1.describe('Exist Test', () => __awaiter(void 0, void 0, void 0, function* () {
    it('Should exist', () => __awaiter(void 0, void 0, void 0, function* () {
        const exist = yield src_1.default.exist({ email: userData.email }).catch((err) => {
            console.error(err);
        });
        chai_1.expect(exist).to.be.true;
    })).timeout(2500);
    it('Should not exist', () => __awaiter(void 0, void 0, void 0, function* () {
        const gen = generate_password_1.generate({
            length: 10,
            numbers: false,
            symbols: false,
            strict: true,
        });
        const exist = yield src_1.default
            .exist({ email: gen + '@' + gen + '.com' })
            .catch((err) => {
            console.error(err);
        });
        chai_1.expect(exist).to.be.false;
    })).timeout(2500);
}));
exports.default = exist;
//# sourceMappingURL=exist.spec.js.map