"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mocha_1 = require("mocha");
const chai_1 = require("chai");
const src_1 = __importDefault(require("../src"));
const utils_1 = require("../src/utils");
const userData = utils_1.getGlobalUser();
const deleted = mocha_1.describe('Delete Test', () => __awaiter(void 0, void 0, void 0, function* () {
    it('Should successfully delete user', () => __awaiter(void 0, void 0, void 0, function* () {
        const deletedUser = yield src_1.default
            .deleteUser({ email: userData.email })
            .catch((err) => {
            console.error(err);
        });
        chai_1.expect(deletedUser).to.be.ok;
    })).timeout(3500);
    it('Should get delete error', () => __awaiter(void 0, void 0, void 0, function* () {
        const code = yield src_1.default
            .deleteUser({ email: userData.email })
            .catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('P2025');
    })).timeout(3500);
}));
exports.default = deleted;
//# sourceMappingURL=delete.spec.js.map