"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mocha_1 = require("mocha");
const chai_1 = require("chai");
const src_1 = __importDefault(require("../src"));
const utils_1 = require("../src/utils");
const generate_password_1 = require("generate-password");
const userData = utils_1.getGlobalUser();
const register = mocha_1.describe('Register Test', () => __awaiter(void 0, void 0, void 0, function* () {
    it('Should successfully create user', () => __awaiter(void 0, void 0, void 0, function* () {
        const user = yield src_1.default.register(userData).catch((err) => {
            console.error(err);
        });
        chai_1.expect(user).to.be.ok;
    })).timeout(6000);
    it('Should get unique email error', () => __awaiter(void 0, void 0, void 0, function* () {
        const code = yield src_1.default.register(userData).catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('012');
    })).timeout(2000);
    it('Should get email format error', () => __awaiter(void 0, void 0, void 0, function* () {
        const wrongUserData = Object.assign({}, userData);
        wrongUserData.email = generate_password_1.generate({
            length: 5,
            numbers: true,
            symbols: false,
        });
        const code = yield src_1.default.register(wrongUserData).catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('004');
    })).timeout(1000);
    it('Should get password length error', () => __awaiter(void 0, void 0, void 0, function* () {
        const wrongUserData = Object.assign({}, userData);
        wrongUserData.password = generate_password_1.generate({
            length: 5,
            numbers: true,
            symbols: false,
        });
        const code = yield src_1.default.register(wrongUserData).catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('005');
    })).timeout(1000);
    it('Should get password format error', () => __awaiter(void 0, void 0, void 0, function* () {
        const wrongUserData = Object.assign({}, userData);
        wrongUserData.password = generate_password_1.generate({
            length: 8,
            numbers: false,
            symbols: false,
            strict: true,
        });
        const code = yield src_1.default.register(wrongUserData).catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('006');
    })).timeout(1000);
    it('Should get name length error', () => __awaiter(void 0, void 0, void 0, function* () {
        const wrongUserData = Object.assign({}, userData);
        wrongUserData.name = generate_password_1.generate({
            length: 31,
            numbers: false,
            symbols: false,
        });
        const code = yield src_1.default.register(wrongUserData).catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('007');
    })).timeout(1000);
    it('Should get last name length error', () => __awaiter(void 0, void 0, void 0, function* () {
        const wrongUserData = Object.assign({}, userData);
        wrongUserData.last_name = generate_password_1.generate({
            length: 31,
            numbers: false,
            symbols: false,
        });
        const code = yield src_1.default.register(wrongUserData).catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('008');
    })).timeout(1000);
}));
exports.default = register;
//# sourceMappingURL=register.spec.js.map