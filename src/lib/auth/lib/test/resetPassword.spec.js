"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mocha_1 = require("mocha");
const chai_1 = require("chai");
const src_1 = __importDefault(require("../src"));
const utils_1 = require("../src/utils");
const generate_password_1 = require("generate-password");
const uuid_1 = require("uuid");
const userData = utils_1.getGlobalUser();
const reset = mocha_1.describe('Reset password Test', () => __awaiter(void 0, void 0, void 0, function* () {
    let correctCode = '';
    it('Should request reset url', () => __awaiter(void 0, void 0, void 0, function* () {
        correctCode = (yield src_1.default.resetPasswordByUrl(userData.email).catch((err) => {
            console.error(err);
        }));
        chai_1.expect(correctCode).to.be.string;
    })).timeout(4000);
    it('Should get incorrect email format error', () => __awaiter(void 0, void 0, void 0, function* () {
        const code = yield src_1.default
            .resetPasswordByUrl(generate_password_1.generate({
            length: 10,
            symbols: false,
        }))
            .catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('018');
    })).timeout(1000);
    it('Should validate code successfully', () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield src_1.default.validateResetCode(correctCode).catch((err) => {
            return err.code;
        });
        chai_1.expect(response).to.be.ok;
    })).timeout(2000);
    it('Should get incorrect code format', () => __awaiter(void 0, void 0, void 0, function* () {
        const code = yield src_1.default
            .validateResetCode(generate_password_1.generate({
            length: 36,
        }))
            .catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('019');
    }));
    it('Should get code not found', () => __awaiter(void 0, void 0, void 0, function* () {
        const code = yield src_1.default.validateResetCode(uuid_1.v4()).catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('020');
    })).timeout(1000);
    it('Should get password password length error', () => __awaiter(void 0, void 0, void 0, function* () {
        const code = yield src_1.default
            .changePassword(correctCode, generate_password_1.generate({
            numbers: true,
            length: 5,
        }))
            .catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('025');
    })).timeout(1000);
    it('Should get password password format error', () => __awaiter(void 0, void 0, void 0, function* () {
        const code = yield src_1.default
            .changePassword(correctCode, generate_password_1.generate({
            numbers: false,
            length: 9,
            strict: true,
        }))
            .catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('026');
    }));
    it('Should get code format error', () => __awaiter(void 0, void 0, void 0, function* () {
        const code = yield src_1.default
            .changePassword(generate_password_1.generate({
            numbers: true,
            length: 9,
            strict: true,
        }), generate_password_1.generate({
            numbers: true,
            length: 9,
            strict: true,
        }))
            .catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('022');
    }));
    it('Should get code not found error', () => __awaiter(void 0, void 0, void 0, function* () {
        const code = yield src_1.default
            .changePassword(uuid_1.v4(), generate_password_1.generate({
            numbers: true,
            length: 9,
            strict: true,
        }))
            .catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('023');
    })).timeout(2000);
    it('Should change password successfully', () => __awaiter(void 0, void 0, void 0, function* () {
        const user = yield src_1.default
            .changePassword(correctCode, userData.password)
            .catch((err) => {
            return err.code;
        });
        chai_1.expect(user).to.be.ok;
    })).timeout(5000);
}));
exports.default = reset;
//# sourceMappingURL=resetPassword.spec.js.map