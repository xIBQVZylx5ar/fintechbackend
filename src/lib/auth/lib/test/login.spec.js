"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mocha_1 = require("mocha");
const generate_password_1 = require("generate-password");
const chai_1 = require("chai");
const src_1 = __importDefault(require("../src"));
const utils_1 = require("../src/utils");
const userData = utils_1.getGlobalUser();
const login = mocha_1.describe('Login Test', () => __awaiter(void 0, void 0, void 0, function* () {
    it('Should successfully authenticate', () => __awaiter(void 0, void 0, void 0, function* () {
        const user = yield src_1.default
            .login(userData.email, userData.password)
            .catch((err) => {
            console.error(err);
        });
        chai_1.expect(user).to.be.ok;
    })).timeout(2500);
    it('Should get incorrect password', () => __awaiter(void 0, void 0, void 0, function* () {
        const password = generate_password_1.generate({
            length: 8,
            numbers: true,
            symbols: false,
            strict: true,
        });
        const code = yield src_1.default.login(userData.email, password).catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('003');
    })).timeout(2500);
    it('Should get email format error', () => __awaiter(void 0, void 0, void 0, function* () {
        const code = yield src_1.default
            .login(generate_password_1.generate({ length: 8, numbers: true, symbols: false }), generate_password_1.generate({ length: 8, numbers: true, symbols: false }))
            .catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('009');
    })).timeout(500);
    it('Should get length error', () => __awaiter(void 0, void 0, void 0, function* () {
        const code = yield src_1.default
            .login(userData.email, generate_password_1.generate({ length: 7, numbers: true, symbols: false }))
            .catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('010');
    })).timeout(500);
    it('Should get password format error', () => __awaiter(void 0, void 0, void 0, function* () {
        const code = yield src_1.default
            .login(userData.email, generate_password_1.generate({ length: 8, numbers: false, symbols: false }))
            .catch((err) => {
            return err.code;
        });
        chai_1.expect(code).to.be.equal('011');
    })).timeout(500);
}));
exports.default = login;
//# sourceMappingURL=login.spec.js.map