# Bananacode Auth Package


![TEST CI](https://github.com/bananacode-co/auth/workflows/TEST%20CI/badge.svg)



## How to install 

* First : You should be able to see the bananacode auth npm private package to be able to downloaded

* Second: You need to request a  **token** from your Github profile and give read access to Github packages, note that this is different from you ssh key access don't try to pull the package using ssh otherwise the installation may fail

  https://github.com/settings/tokens

* Third: You have to create a file called **.npmrc** within you project root folder and add the following lines 

  ```
  @bananacode-co:registry=https://npm.pkg.github.com/
  //npm.pkg.github.com/:_authToken=[replace with your token without the brackets]
  ```

* Fourth: Install the package normally and follow the command line instructions 

  ```bash
   npm i @bananacode-co/auth
  ```

* Fifth: Once you have everything setup correctly go and do a test with the user you just created via command line by creating the following code . Remember to replace the shown examples with the ones you created

  #### Javascript

  ```javascript
  const auth = require('@bananacode-co/auth')
  
  auth.login('avalenciano@bananacode.co','password')
  .then(user=>console.log(user)).catch(err=>console.error(err))
  ```

  #### Typescript

  ```typescript
  import auth from '@bananacode-co/auth';
  
  auth.login('avalenciano@bananacode.co','password')
  .then(user=>console.log(user)).catch(err=>console.error(err))
  ```




## Functions  

| Name               | Description                                                  | PostgreSQL | MongoDB |
| ------------------ | ------------------------------------------------------------ | ---------- | ------- |
| register           | Allow the creation of an user with a welcome email message if configured | ✔          | ✘       |
| login              | Allow login with a specified email and password              | ✔          | ✘       |
| exist              | Ask if an user exist in the database using either id or email if both are provided will use "AND" clause to validate both | ✔          | ✘       |
| deleteUser         | Delete an user int the database using either id or email if both are provided will use "AND" clause to validate both if user doesn't exist will return an error | ✔          | ✘       |
| resetPasswordByUrl | If configured user should receive an email with the specified URL with a query Param "code" to validate on **validateResetCode** function | ✔          | ✘       |
| validateResetCode  | Will take code from query Param to validate if the code is valid (code is valid for 5 minutes) | ✔          | ✘       |
| changePassword     | Will take code and password and should successfully change the password (code is valid for 5 minutes) | ✔          | ✘       |

## TODO

| Section  | Task                                                         |
| -------- | ------------------------------------------------------------ |
| Config   | Add support for **windows** computers using path resolve and path join to locate files |
| Database | Add files to support **mongodb** configurations              |
|          |                                                              |

## AuthError

When running the **Bananacode** authentication package, the errors are handle by an object called **AuthError** that error means that exception is being handle by the package otherwise it's an error coming from the database,inputs,queries either **Prisma** library or **mongoose** connector .

The **AuthError** should show the message to help on how to fix the error in any case this documentation will be expanded to handle as much errors as possible if you have access to this repository feel free to make any suggestion or PR to extend a function implement a new feature or something similar,also errors can be submitted via Github [issues]("https://github.com/bananacode-co/auth/issues") .

If you find error code similar to this format 'P-000' take a look into **Prisma** errors documentation  https://www.prisma.io/docs/concepts/components/prisma-client/error-reference#error-codes

| Code | Section  | Message                                                  | Description                                                  |
| ---- | -------- | :------------------------------------------------------- | :----------------------------------------------------------- |
| 001  | Config   | Database engine should be specified check [path]         | When the engine wasn't configure correctly via command line  |
| 002  | Config   | Correct database engine should be specified check [path] | When the engine wasn't configure correctly via command line  |
| 003  | Login    | Incorrect password                                       | This means that user was found in the database but the inserted password is incorrect |
| 004  | Register | Incorrect email format                                   | This means the given input for field email is in a bad format |
| 005  | Register | Password must be at least 8 characters                   | This means password should be 8 or more characters           |
| 006  | Register | Password must contain numbers and letters                | This means password should have numbers and letters          |
| 007  | Register | Name max length is 30 characters                         | This means the maximum character length for name is 30       |
| 008  | Register | Last name max length is 30 characters                    | This means the maximum character length for name is 30       |
| 009  | Login    | Incorrect email format                                   | This means the given input for field email is in a bad format |
| 010  | Login    | Password must be at least 8 characters                   | This means password should be 8 or more characters           |
| 011  | Login    | Password must contain numbers and letters                | This means password should have numbers and letters          |
| 012  | Register | Email has to be unique                                   | This means that user email was already in the database       |
| 013  | Delete   | Specified user does not exist                            | This means that the user doesn't exist into the database     |
| 014  | Email    | One of the emails specified has an incorrect format      | This means email reg-ex failed on specified recipients       |
| 015  | Config   | Specification of SMPT should be at least false           | This means configuration is missing the boolean attribute of SMTP which should be false or true not undefined |
| 016  | Reset    | Mail configuration is not set                            | This means when package was configured mail wasn't set to true winch invalidates the password recovery function |
| 017  | Reset    | Exceeded maximum requests                                | This means password recovery function has been called more than 5 times |
| 018  | Reset    | Incorrect email format                                   | This means the given input for field email is in a bad format |
| 019  | Validate | Incorrect code format                                    | This means the given input for field code is in a bad format |
| 020  | Validate | Code not found                                           | This means the code query couldn't find any record           |
| 021  | Validate | Code has expired                                         | This means the code wasn't retrieve within 5 minutes         |
| 022  | Change   | Incorrect code format                                    | This means the given input for field code is in a bad format |
| 023  | Change   | Code not found                                           | This means the code query couldn't find any record           |
| 024  | Change   | Code has expired                                         | This means the code wasn't retrieve within 5 minutes         |
| 025  | Change   | Password must be at least 8 characters                   | This means password should be 8 or more characters           |
| 026  | Change   | Password must contain numbers and letters                | This means password should have numbers and letters          |

