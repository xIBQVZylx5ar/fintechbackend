import prisma from '../../utils/prisma';
import { ExistOptions } from '../../@types';

const exist = async ({ id, email }: ExistOptions): Promise<boolean> => {
	const count = await prisma.user
		.count({ where: { id, email } })
		.catch((err) => {
			throw err;
		});
	if (count === 0) {
		return false;
	}

	return true;
};

export default exist;
