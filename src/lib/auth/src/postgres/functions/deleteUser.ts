import { formatUser } from '../../utils';
import { DeleteOptions, User } from '../../@types';
import prisma from '../../utils/prisma';
import AuthError from '../../classes/AuthError';
const deleteUser = async ({ id, email }: DeleteOptions): Promise<User> => {
	const deleted = await prisma.user
		.delete({
			where: {
				id,
				email,
			},
		})
		.catch((err) => {
			if (err?.code === 'P2016') {
				throw new AuthError('Specified user does not exist', '013');
			}
			throw err;
		});
	return formatUser(deleted);
};
export default deleteUser;
