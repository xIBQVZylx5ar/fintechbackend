import { EmailExtraOptions } from 'src/@types';
import AuthError from '../../classes/AuthError';
import { mailer, getEnviromentals, prisma } from '../../utils';
import { v4 as uuid } from 'uuid';

import validator from 'validator';
const {
	USE_MAILGUN,
	SMTP,
	DOMAIN,
	USE_MAIL,
	PASSWORD_PATH,
} = getEnviromentals();
//Pass id options to make query an udpdate go faster
const resetPasswordByUrl = async (
	email: string,
	id?: number,
	options?: EmailExtraOptions
): Promise<string> => {
	if (!validator.isEmail(email))
		throw new AuthError('Incorrect email format', '018');
	if (!USE_MAIL) throw new AuthError('Mail configuration is not set', '016');
	let user_id: number;
	if (id) {
		user_id = id;
	} else {
		const user = await prisma.user
			.findUnique({ where: { email: email } })
			.catch((err) => {
				throw err;
			});
		user_id = user?.id || 0;
	}
	const existCode = await prisma.code.findUnique({
		where: {
			user_id,
		},
	});
	const code = uuid();
	if (existCode) {
		if (existCode.requests < 5) {
			await prisma.code
				.update({
					where: { user_id },
					data: {
						created_at: new Date(Date.now()),
						code,
						requests: existCode.requests + 1,
					},
				})
				.catch((err) => {
					throw err;
				});
		} else {
			const now = new Date().getTime();
			const diff = Math.floor(now - existCode.created_at.getTime());
			const minutes = Math.round(diff / 1000 / 60);
			if (minutes < 60) {
				throw new AuthError('Exceeded maximum requests', '017');
			} else {
				await prisma.code
					.update({
						where: { user_id },
						data: {
							code,
							requests: 1,
						},
					})
					.catch((err) => {
						throw err;
					});
			}
		}
	} else {
		await prisma.code.create({
			data: {
				code,
				created_at: new Date(Date.now()),
				requests: 1,
				user_id,
				//TODO: use connect then prisma 2 error has been fixed 'P2014' on prisma docs https://github.com/prisma/prisma/discussions/2610
				// user: {
				// 	connect: {
				// 		id: user_id,
				// 	},
				// },
			},
		});
	}

	const link = `https://${DOMAIN}/${PASSWORD_PATH}?code=${code}`;
	const from = options?.from ? options.from : `NO REPLY <no-reply@${DOMAIN}>`;
	const subject = options?.subject ? options.subject : `Recuperación`;
	const variables = options?.template?.variables
		? options.template.variables
		: {
				title: 'Contraseña',
				body: 'Ha solicitado un cambio de contraseña',
				footer: 'Link válido por 5 minutos',
				callToAction: 'Cambiar contraseña',
				link,
		  };
	const text = options?.text
		? options.text
		: `Cambio de contraseña ${link} \n Expira en 5 minutos`;
	const templateName = options?.template?.name
		? options.template.name
		: 'welcome';
	if (USE_MAILGUN) {
		mailer
			.sendTemplate({
				from,
				to: [email],
				subject: subject,
				variables: variables,
				template: templateName,
			})
			.then()
			.catch((err: Error) => console.error(err));
	} else if (SMTP) {
		mailer
			.sendSimple({
				from: from,
				to: [email],
				subject: subject,
				text: text,
			})
			.then()
			.catch((err: Error) => console.error(err));
	}
	return code;
};
export default resetPasswordByUrl;
