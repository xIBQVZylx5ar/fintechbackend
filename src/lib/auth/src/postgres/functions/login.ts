import validator from 'validator';
import { User } from '../../@types';
import prisma from '../../utils/prisma';
import { hasher, formatUser } from '../../utils';
import AuthError from '../../classes/AuthError';

const login = async (
	email: string,
	password: string
): Promise<User | undefined> => {
	if (!validator.isEmail(email))
		throw new AuthError('Incorrect email format', '009');
	if (password.length < 8)
		throw new AuthError('Password must be at least 8 characters', '010');
	if (validator.isAlpha(password) || validator.isNumeric(password))
		throw new AuthError('Password must contain numbers and letters', '011');
	const user = await prisma.user
		.findUnique({
			where: {
				email: email.toLowerCase(),
			},
		})
		.catch((err) => {
			throw err;
		});
	if (!user) return;
	if (!(await hasher.comparePassword(password, user.password)))
		throw new AuthError(`Incorrect Password`, '003');

	return formatUser(user);
};

export default login;
