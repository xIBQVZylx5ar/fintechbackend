import AuthError from '../../classes/AuthError';
import * as uuid from 'uuid';
import { prisma } from '../../utils';

const validateResetCode = async (code: string): Promise<boolean> => {
	if (!uuid.validate(code)) throw new AuthError('Incorrect code format', '019');
	const recovery = await prisma.code
		.findUnique({
			where: {
				code,
			},
		})
		.catch((err) => {
			throw err;
		});
	if (!recovery) throw new AuthError('Code not found', '020');
	const now = new Date().getTime();
	const diff = Math.floor(now - recovery.created_at.getTime());
	const minutes = Math.round(diff / 1000 / 60);
	if (minutes > 5) throw new AuthError('Code has expired', '021');
	return true;
};
export default validateResetCode;
