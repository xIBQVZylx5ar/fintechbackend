import prisma from '../../utils/prisma';
import validator from 'validator';
import { RegisterOptions, User, RegisterExtraOptions } from '../../@types';
import { hasher, formatUser, mailer } from '../../utils';
import AuthError from '../../classes/AuthError';
import { getEnviromentals } from '../../utils';

const {
	USE_MAIL,
	USE_MAILGUN,
	SMTP,
	SENDER_NAME,
	SENDER_EMAIL,
	DOMAIN,
	LOGIN_PATH,
} = getEnviromentals();
const register = async (
	{
		birth_date,
		organization,
		status,
		user_type,
		password,
		email,
		last_name,
		name,
	}: RegisterOptions,
	options?: RegisterExtraOptions
): Promise<User> => {
	if (name.length > 30)
		throw new AuthError('Name max length is 30 characters', '007');
	if (last_name.length > 30)
		throw new AuthError('Last name max length is 30 characters', '008');
	if (!validator.isEmail(email))
		throw new AuthError('Incorrect email format', '004');
	if (password.length < 8)
		throw new AuthError('Password must be at least 8 characters', '005');
	if (validator.isAlpha(password) || validator.isNumeric(password))
		throw new AuthError('Password must contain numbers and letters', '006');
	const birth = birth_date ? new Date(birth_date) : null;
	const hashedPassword = await hasher.hashPassword(password);

	const user = await prisma.user
		.create({
			data: {
				birth_date: birth,
				organization: organization.toLowerCase(),
				status,
				user_type: user_type.toLowerCase(),
				password: hashedPassword,
				email,
				last_name,
				name,
			},
		})
		.catch((err) => {
			if (err?.code === 'P2002') {
				throw new AuthError('Email has to be unique', '012');
			}
			throw err;
		});
	const formattedUser = formatUser(user);
	if (USE_MAIL) {
		const link = `https://${DOMAIN}/${LOGIN_PATH}`;
		const from = options?.email?.from
			? options.email.from
			: `${SENDER_NAME} <${SENDER_EMAIL}>`;

		const subject = options?.email?.subject
			? options.email.subject
			: `Bienvenido`;
		const variables = options?.email?.template?.variables
			? options.email.template.variables
			: {
					title: 'Bienvenido',
					body: 'Te damos la bienvenida a la paltaforma',
					footer: 'Gracias por creer en nosotros',
					callToAction: 'Inicar sesion',
					link,
			  };
		const text = options?.email?.text
			? options.email.text
			: `Hola ${user.name} te damos la bienvenida a la plataforma inicia sesión ${link}`;
		const templateName = options?.email?.template?.name
			? options.email.template.name
			: 'welcome';
		if (USE_MAILGUN) {
			mailer
				.sendTemplate({
					from,
					to: [formattedUser.email],
					subject: subject,
					variables: variables,
					template: templateName,
				})
				.then()
				.catch((err: Error) => console.error(err));
		} else if (SMTP) {
			mailer
				.sendSimple({
					from: from,
					to: [formattedUser.email],
					subject: subject,
					text: text,
				})
				.then()
				.catch((err: Error) => console.error(err));
		}
	}
	return formattedUser;
};

export default register;
