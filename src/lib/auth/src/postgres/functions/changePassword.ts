import { User } from '../../@types';
import * as uuid from 'uuid';
import { hasher, prisma, formatUser } from '../../utils';
import AuthError from '../../classes/AuthError';
import validator from 'validator';
const changePassword = async (
	code: string,
	password: string
): Promise<User> => {
	if (password.length < 8)
		throw new AuthError('Password must be at least 8 characters', '025');
	if (validator.isAlpha(password) || validator.isNumeric(password))
		throw new AuthError('Password must contain numbers and letters', '026');

	if (!uuid.validate(code)) throw new AuthError('Incorrect code format', '022');
	const recovery = await prisma.code
		.findUnique({
			where: {
				code,
			},
		})
		.catch((err) => {
			throw err;
		});
	if (!recovery) throw new AuthError('Code not found', '023');
	const now = new Date().getTime();
	const diff = Math.floor(now - recovery.created_at.getTime());
	const minutes = Math.round(diff / 1000 / 60);
	if (minutes > 5) throw new AuthError('Code has expired', '024');
	prisma.code
		.delete({
			where: {
				code,
			},
		})
		.catch((err) => {
			throw err;
		});
	const newPassword = await hasher.hashPassword(password);
	const user = await prisma.user
		.update({
			where: {
				id: recovery.user_id,
			},
			data: {
				password: newPassword,
			},
		})
		.catch((err) => {
			throw err;
		});
	return formatUser(user);
};

export default changePassword;
