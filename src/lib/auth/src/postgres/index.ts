import login from './functions/login';
import exist from './functions/exist';
import register from './functions/register';
import deleteUser from './functions/deleteUser';
import resetPasswordByUrl from './functions/resetPasswordByUrl';
import validateResetCode from './functions/validateResetCode';
import changePassword from './functions/changePassword';
export default {
	login,
	exist,
	register,
	deleteUser,
	resetPasswordByUrl,
	validateResetCode,
	changePassword,
};
