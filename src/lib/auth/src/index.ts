import { Auth } from './@types';
import AuthError from './classes/AuthError';
import postgres from './postgres';
//import mongodb from './mongodb';
import { getEnviromentals } from './utils';

const { ENGINE } = getEnviromentals();
const auth = (): Auth => {
	if (!ENGINE) {
		throw new AuthError(
			`Database engine should be specified check the configuration path`,
			'001'
		);
	}

	const engine: string = ENGINE || '';
	if (engine === 'postgres' || engine === 'mysql') return postgres;
	//TODO change for mongodb connector;
	else if (engine === 'mongodb') return postgres;
	else
		throw new AuthError(
			`Correct database engine should be specified check  configuration path`,
			'002'
		);
};

export = auth();
