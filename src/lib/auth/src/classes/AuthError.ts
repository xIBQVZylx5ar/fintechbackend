/* eslint-disable no-console */

class AuthError extends Error {
	public code: string;
	public msg: string;
	constructor(message: string, code: string) {
		const log =
			'\n•----------------------•\n' +
			message +
			'\n' +
			'You can check this error into the documentation ' +
			'Error code: ' +
			code +
			'\n' +
			'https://github.com/bananacode-co/auth/blob/master/README.md' +
			'\n•----------------------•\n';
		('\n');
		super(log);
		// console.log(
		// 	'You can check this error into the documentation ' + 'Error code: ' + code
		// );
		// console.log('https://github.com/bananacode-co/auth/blob/master/README.md');
		this.code = code;
		this.msg = message;
		Object.setPrototypeOf(this, AuthError.prototype);
	}
}

export default AuthError;
