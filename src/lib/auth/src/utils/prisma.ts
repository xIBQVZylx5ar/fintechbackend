import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient({});
export const closeConnection = (): void => {
	prisma.$disconnect();
};
export default prisma;
