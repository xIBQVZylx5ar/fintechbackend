import bcrypt from 'bcrypt';

const env = process.env.NODE_ENV || 'developement';
const saltrounds = env === 'development' ? 5 : 10;
const hashPassword = (password: string): Promise<string> => {
	return bcrypt.hash(password, saltrounds);
};
const comparePassword = (
	password: string,
	hashedPassword: string
): Promise<boolean> => {
	return bcrypt.compare(password, hashedPassword);
};
export default { hashPassword, comparePassword };
