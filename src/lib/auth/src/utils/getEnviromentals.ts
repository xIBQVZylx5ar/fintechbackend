import { config as dot, DotenvConfigOutput } from 'dotenv';
import { Enviromentals } from 'src/@types';
import AuthError from '../classes/AuthError';
let envPath = __dirname + '/../../../prisma/.env';

const getEnviromentals = (): Enviromentals => {
	let dotConfig: DotenvConfigOutput = dot({
		path: envPath,
	});
	if (!dotConfig.parsed?.USE_MAIL) {
		//Retry when run is in development mode
		envPath = __dirname + '/../../prisma/.env';
		dotConfig = dot({
			path: envPath,
		});
		if (!dotConfig.parsed?.USE_MAIL) {
			throw new AuthError(
				`Specification of SMPT should be at least false ` + envPath,
				'015'
			);
		}
	}
	return {
		USE_MAIL:
			process.env.USE_MAIL === 'true' || dotConfig.parsed.USE_MAIL === 'true',
		MAILGUN_API_KEY:
			process.env.MAILGUN_API_KEY || dotConfig.parsed.MAILGUN_API_KEY,
		MAILGUN_DOMAIN:
			process.env.MAILGUN_DOMAIN || dotConfig.parsed.MAILGUN_DOMAIN,
		USE_MAILGUN:
			process.env.USE_MAILGUN === 'true' ||
			dotConfig.parsed.USE_MAILGUN === 'true',
		SMTP: process.env.SMTP === 'true' || dotConfig.parsed.SMTP === 'true',
		SMTP_URL: process.env.SMTP_URL || dotConfig.parsed.SMTP_URL,
		SMTP_SECURE:
			process.env.SMTP_SECURE === 'true' ||
			dotConfig.parsed.SMTP_SECURE === 'true',
		NODE_ENV: process.env.NODE_ENV || 'ddevelopment',
		ENGINE: process.env.ENGINE || dotConfig.parsed.ENGINE,
		DATABASE_URL: process.env.DATABASE_URL || dotConfig.parsed.DATABASE_URL,
		SENDER_EMAIL:
			process.env.SENDER_EMAIL || dotConfig.parsed.SENDER_EMAIL
				? dotConfig.parsed.SENDER_EMAIL
				: '',
		SENDER_NAME:
			process.env.SENDER_NAME || dotConfig.parsed.SENDER_NAME
				? dotConfig.parsed.SENDER_NAME
				: '',
		DOMAIN:
			process.env.DOMAIN || dotConfig.parsed.DOMAIN
				? dotConfig.parsed.DOMAIN
				: 'notset.com',
		PASSWORD_PATH:
			process.env.PASSWORD_PATH || dotConfig.parsed.PASSWORD_PATH
				? dotConfig.parsed.PASSWORD_PATH
				: 'recovery',
		LOGIN_PATH:
			process.env.LOGIN_PATH || dotConfig.parsed.LOGIN_PATH
				? dotConfig.parsed.LOGIN_PATH
				: 'login',
	};
};
export default getEnviromentals;
