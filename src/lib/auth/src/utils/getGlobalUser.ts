import { UserStatus } from 'src/@types';
import { user } from '../config/config.json';

const env = process.env.NODE_ENV || 'development';
export interface UserData {
	birth_date: number;
	organization: string;
	status: UserStatus;
	user_type: string;
	password: string;
	email: string;
	last_name: string;
	name: string;
}
const userConfig = {
	name: user.name,
	birth_date: user.birth_date,
	organization: user.organization,
	status: <UserStatus>user.status,
	user_type: user.user_type,
	password: user.password,
	email: user.email,
	last_name: user.last_name,
};

const getUserConfig = (): UserData => {
	if (env === 'development') {
		return userConfig;
	}
	return {
		birth_date: process.env.USER_BIRTH_DATE
			? Number.parseInt(process.env.USER_BIRTH_DATE)
			: user.birth_date,
		organization: process.env.USER_ORGANIZATION || user.organization,
		status: <UserStatus>process.env.USER_STATUS || <UserStatus>user.status,
		user_type: process.env.USER_TYPE || user.user_type,
		password: process.env.USER_PASSWORD || user.password,
		email: process.env.USER_EMAIL || user.email,
		last_name: process.env.USER_LAST_NAME || user.last_name,
		name: process.env.USER_NAME || user.name,
	};
};
export default getUserConfig;
