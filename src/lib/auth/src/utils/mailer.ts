import nodemailer, { SentMessageInfo } from 'nodemailer';
import { SendSimpleOptions, TemplateOptions } from '../@types/index';
import validator from 'validator';
import AuthError from '../classes/AuthError';
import getEnviromentals from './getEnviromentals';
import Mail from 'nodemailer/lib/mailer';
import mailgunjs from 'mailgun-js';

const {
	SMTP,
	SMTP_URL,
	SMTP_SECURE,
	MAILGUN_API_KEY,
	MAILGUN_DOMAIN,
} = getEnviromentals();
const mailgun = mailgunjs({
	apiKey: MAILGUN_API_KEY,
	domain: MAILGUN_DOMAIN,
});

const getTransport = (): Mail | undefined => {
	let transporter;
	try {
		transporter = nodemailer.createTransport(SMTP_URL?.toString(), {
			secure: SMTP_SECURE,
		});
	} catch (error) {
		transporter = undefined;
	}
	return transporter;
};

const sendSimple = async ({
	from,
	to,
	subject,
	text,
}: SendSimpleOptions): Promise<SentMessageInfo> => {
	const transporter = getTransport();
	if (!SMTP || !transporter) return;
	to.forEach((recipient) => {
		if (!validator.isEmail(recipient)) {
			throw new AuthError(
				'One of the emails specified has an incorrect format',
				'014'
			);
		}
	});
	const recipients = to.join(',');
	const info = await transporter
		.sendMail({
			from: from,
			to: recipients,
			subject,
			text,
		})
		.catch((err: Error) => {
			throw err;
		});
	return info;
};

const sendTemplate = async ({
	to,
	template,
	from,
	subject,
	variables,
}: TemplateOptions): Promise<mailgunjs.messages.SendResponse | undefined> => {
	to.forEach((recipient) => {
		if (!validator.isEmail(recipient)) {
			throw new AuthError(
				'One of the emails specified has an incorrect format',
				'014'
			);
		}
	});
	const recipients = to.join(',');
	const data = {
		from: from,
		to: recipients,
		subject,
		template,
		variables,
		'h:X-Mailgun-Variables': JSON.stringify(variables),
	};
	const response = await mailgun.messages().send(data, (error, body) => {
		if (error) {
			console.error(error);
			return;
		} else {
			return body;
		}
	});
	return response;
};
export default {
	sendSimple,
	sendTemplate,
};
