import { user } from '@prisma/client';
import { User, UserStatus } from '../@types';

const formatUser = (user: user): User => {
	return {
		id: user.id,
		name: user.name,
		last_name: user.last_name,
		birth_date: user.birth_date,
		status: <UserStatus>user.status,
		created_at: user.created_at,
		email: user.email,
		organization: user.organization,
		password: user.password,
		user_type: user.user_type,
	};
};
export default formatUser;
