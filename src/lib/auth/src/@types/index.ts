export interface User {
	id: number;
	name: string;
	last_name: string;
	email: string;
	password: string;
	birth_date: Date | null;
	created_at: Date;
	status: UserStatus;
	user_type: string;
	organization: string | '@';
}
export interface Response {
	message: 'ok';
}
export interface DeleteOptions {
	id?: number;
	email?: string;
}
export interface ExistOptions {
	id?: number;
	email?: string;
}
export interface RegisterOptions {
	name: string;
	last_name: string;
	email: string;
	password: string;
	birth_date: number | undefined;
	status: UserStatus;
	user_type: string;
	organization: string | '@';
}
export interface RegisterExtraOptions {
	email?: EmailExtraOptions;
}
export interface EmailExtraOptions {
	from?: string;
	subject?: string;
	template?: {
		name: string;
		variables: { [key: string]: string };
	};
	text?: string;
}
export interface TemplateOptions {
	template: string;
	from: string;
	to: [string];
	subject: string;
	variables: { [key: string]: string };
}
export interface Auth {
	login: (email: string, password: string) => Promise<User | undefined>;
	exist: ({ id, email }: ExistOptions) => Promise<boolean>;
	register: (
		user: RegisterOptions,
		options?: RegisterExtraOptions
	) => Promise<User>;
	deleteUser: ({ id, email }: DeleteOptions) => Promise<User>;
	resetPasswordByUrl: (
		email: string,
		id?: number,
		options?: EmailExtraOptions
	) => Promise<string>;
	validateResetCode: (code: string) => Promise<boolean>;
	changePassword: (code: string, password: string) => Promise<User>;
}

export type UserStatus =
	| 'active'
	| 'banned'
	| 'suspended'
	| 'inactive'
	| 'debtor'
	| 'free'
	| 'promotion';

export type Engine = 'mongodb' | 'postgres';

export interface SendSimpleOptions {
	from: string;
	to: [string];
	subject: string;
	text: string;
}
export interface Enviromentals {
	SMTP: boolean | undefined;
	SMTP_URL: string | undefined;
	SMTP_SECURE: boolean | undefined;
	NODE_ENV: string | undefined;
	ENGINE: string | undefined;
	DATABASE_URL: string | undefined;
	USE_MAIL: boolean;
	USE_MAILGUN: boolean;
	MAILGUN_API_KEY: string;
	MAILGUN_DOMAIN: string;
	SENDER_NAME: string;
	SENDER_EMAIL: string;
	DOMAIN: string;
	PASSWORD_PATH: string;
	LOGIN_PATH: string;
}
