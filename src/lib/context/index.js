const jsonwebtoken = require('jsonwebtoken');
const config = require('../../config');
const prisma = require('../../services/prisma');
const cleaner = require('../../utils/cleaner');

const { JWT_SECRET } = config;
const getUser = async (decoded) => {
  const user = await prisma.user.findUnique({
    where: {
      id: decoded.id,
    },
  });
  if (!user) return null;
  return cleaner.cleanUser(user);
};

const decode = (value) => {
  try {
    return jsonwebtoken.verify(value, JWT_SECRET);
  } catch (error) {
    return null;
  }
};

const createContext = async (req, _, next) => {
  const token = req.headers.authorization;
  req.ctx = { user: null };
  if (token) {
    let decoded = decode(token.split(' ')[1]);
    if (decoded) {
      req.ctx.user = await getUser(decoded);
    }
  }
  // eslint-disable-next-line consistent-return
  next();
};

module.exports = createContext;
