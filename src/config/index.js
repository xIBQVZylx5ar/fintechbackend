const env = process.env.NODE_ENV || 'development';
const logger = {
  URI: 'mongodb://<URL>:<PORT>/<DATABASE>',
};
const development = {
  SALTROUNDS: 2,
  JWT_SECRET: '96"YbgLz@r3toluca35',
};
const production = {
  SALTROUNDS: 10,
  JWT_SECRET: '96"YbgLz@r3toluca35',
  IMAGES_URL: 'https://ik.imagekit.io/qc7btzjvbb/catalog/product/',
};
const stage = env === 'development' ? development : production;

module.exports = {
  ...stage,
  LOGGER_URI: logger.URI,
  __PROD__: env === 'production',
  BASE_URL: process.env.BASE_URL || 'https://sandbox.avenida.cr',
};
