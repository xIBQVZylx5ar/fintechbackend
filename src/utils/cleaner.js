const cleanUser = (user) => {
  let securedUser = user;
  delete securedUser.password;
  return securedUser;
};

module.exports = { cleanUser };
