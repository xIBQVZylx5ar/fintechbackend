const isDomain = (text) => {
  const re = new RegExp(
    '(https://(?:www.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9].[^s]{2,}|www.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9].[^s]{2,}|https://(?:www.|(?!www))[a-zA-Z0-9]+.[^s]{2,}|www.[a-zA-Z0-9]+.[^s]{2,})'
  );
  return re.test(text);
};
const isEmpty = (text) => {
  if (text === '' || text === ' ') {
    return true;
  }
  return false;
};

module.exports = { isDomain, isEmpty };
