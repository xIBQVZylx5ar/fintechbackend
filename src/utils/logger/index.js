const winston = require('winston');
require('winston-mongodb');
const { LOGGER_URI, __PROD__ } = require('../../config');

// eslint-disable-next-line no-unused-vars
const options = {
  db: LOGGER_URI,
  options: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  storeHost: true,
  silent: false,
};
let transports = [
  new winston.transports.Console(),
  // new winston.transports.MongoDB(options) //activar para enviar los logs a mongodb
];
if (!__PROD__) {
  transports = [new winston.transports.Console()];
}
// eslint-disable-next-line new-cap
const logger = new winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports,
});
const log = {
  error: ({ message, tags, error, func, extras }) => {
    logger.error({
      message,
      metadata: {
        func,
        tags,
        error: error ? JSON.stringify(error) : null,
        extras: extras ? JSON.stringify(extras) : null,
      },
    });
  },
  info: ({ message, tags, extras, func }) => {
    logger.info({
      message,
      metadata: {
        func,
        tags,
        extras: extras ? JSON.stringify(extras) : null,
      },
    });
  },
};

module.exports = log;
