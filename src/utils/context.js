const jsonwebtoken = require('jsonwebtoken');
const config = require('../../../../config');
const { User } = require('../../../../services');
const prisma = require('../../../../services/prisma');

const { JWT_SECRET } = config;
const getUser = async (decoded) => {
  const user = await prisma.user.findUnique({
    where: {
      id: decoded.id,
    },
    include: {
      store: {
        include: {
          attribute_group: true,
        },
      },
    },
  });
  if (!user) return null;
  return User.toGql(user);
};

const decode = (value) => {
  try {
    return jsonwebtoken.verify(value, JWT_SECRET);
  } catch (error) {
    return null;
  }
};

// eslint-disable-next-line no-unused-vars
const authenticate = async (req, res) => {
  const token = req.headers['x-token'];
  const refresToken = req.headers['x-refresh-token'];
  if (!token) {
    return;
  }
  let decoded = decode(token);

  let user;
  if (decoded) {
    user = await getUser(decoded);
  } else {
    decoded = decode(refresToken);
    if (!decoded) {
      return;
    }
    user = await getUser(decoded);
  }

  // eslint-disable-next-line consistent-return
  return user;
};

module.exports = authenticate;
