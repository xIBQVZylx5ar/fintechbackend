const momment = require('moment-timezone');

const getCostaRicaHour = () => {
  let date = momment.tz('America/Costa_Rica').valueOf();
  let finalDate = new Date(date);
  return finalDate;
};
module.exports = getCostaRicaHour;
