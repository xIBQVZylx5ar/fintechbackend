const fs = require('fs');

const writeLog=(log)=>{
    try {
        const buffer = fs.readFileSync('dev.log');
        const text=buffer.toString();
        const newText = text+'\n'+'*-----------------------------------------------*'+'\n'+log;
        fs.writeFileSync('dev.log',newText);
    } catch (error) {
        fs.writeFileSync('dev.log',log);
    }
   
    
}
module.exports = writeLog