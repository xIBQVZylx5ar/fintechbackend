const config = require('../config');

const { __PROD__ } = config;
const { Agent } = require('../services/agent');

const AgentMiddleware = async (req, res, next) => {
  if (!__PROD__) {
    return next();
  }
  try {
    console.log(req.headers);
    console.log(req.body);
    const api_key = req.headers['api-key'];
    const api_token = req.headers['api-token'];
    const agent = await Agent.findByApiKey(api_key);
    if (!agent) {
      return res.status(401).json({ message: 'Llaves incorrectas' });
    }
    if (api_token === agent.api_token) {
      return next();
    }
  } catch (error) {
    return res.status(401).json({ message: 'Llaves incorrectas' });
  }
  return res.status(401).json({ message: 'Llaves incorrectas' });
};
module.exports = AgentMiddleware;
