const bcrypt = require('bcryptjs');
const { SALTROUNDS } = require('../config');

const hashPassword = async (password) => {
  return bcrypt.hash(password, SALTROUNDS);
};
const comparePassword = async (password, hashedPassword) => {
  return bcrypt.compare(password, hashedPassword);
};
module.exports = { hashPassword, comparePassword };
