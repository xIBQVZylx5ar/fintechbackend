const getCostaRicaHour = require('./getCostaRicaHour');
const agent = require('./agent');
const hash = require('./hash');
const magento = require('./magento');
const validation = require('./validation');
const dateToCostaRica = require('./dateToCostaRica');
const writeLog = require('./writeLog');

module.exports = {
  dateToCostaRica,
  getCostaRicaHour,
  agent,
  hash,
  magento,
  ...validation,
  writeLog
};
