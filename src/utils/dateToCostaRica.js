const momment = require('moment-timezone');

const dateToCostaRica = (date) => {
  let newDate = momment(date).tz('America/Costa_Rica').format('DD/MM/YYYY');
  return newDate;
};
module.exports = dateToCostaRica;
