const prisma = require('./src/services/prisma');

const start = async () => {
  const user = await prisma.user.create({
    data: {
      email: 'avalenciano@bananacode.co',
      last_name: 'valenciano',
      organization: '@',
      name: 'Alexis',
      user_type: 'cliente',
      status: 'live',
      password: '01231231',
    },
  });
  console.log(user);
  const newUser = await prisma.user.findFirst({
    include: {
      ejemplo: true,
    },
  });
  console.log(newUser);
};
start();
