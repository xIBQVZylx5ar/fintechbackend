/* eslint-disable prettier/prettier */
/* eslint-disable quotes */
module.exports = {
  tabWidth: 2,
  semi: true,
  singleQuote: true,
  trailingComma: 'es5',
};
